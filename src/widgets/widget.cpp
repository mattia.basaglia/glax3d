#include "glax3d/widgets/widget.h"
#include "glax3d/widgets/events.h"

using namespace glax3d;
using namespace glax3d::widgets;


void Widget::mouse_event(const MouseEvent& ev)
{
    if ( ev.type == QEvent::MouseButtonPress )
    {
        set_focus(true);
        mouse_press_event(ev);
    }
    else if ( ev.type == QEvent::MouseButtonRelease )
    {
        mouse_release_event(ev);
    }
    else if ( ev.type == QEvent::MouseMove )
    {
        mouse_move_event(ev);
    }
}

void Widget::set_focus(bool focused)
{
    if ( focused != _focused )
    {
        _focused = focused;
        on_focus(_focused);
        emit focus_changed(_focused);
    }
}
