#include "glax3d/widgets/widget_container.h"

#include "glax3d/widgets/events.h"

using namespace glax3d::widgets;
using namespace glax3d;

namespace glax3d::widgets {
class WidgetContainerPrivate
{
public:
    QVector<Widget*> children;
    Widget* active = nullptr;
    Widget* focused = nullptr;

};
} // namespace glax3d::widgets

WidgetContainer::WidgetContainer()
    : d(std::make_unique<WidgetContainerPrivate>())
{}

WidgetContainer::~WidgetContainer() = default;



void WidgetContainer::append(Widget* child)
{
    d->children.append(child);
    connect(child, &Widget::focus_changed, this, &WidgetContainer::focus_test);
    connect(child, &Widget::widget_destroyed, this, &WidgetContainer::remove);
}

void WidgetContainer::add_child(Widget* child)
{
    child->setParent(this);
    append(child);
}

void WidgetContainer::remove(Widget* child)
{
    d->children.removeAll(child);
    if ( child == d->active )
        d->active = nullptr;
    if ( child == d->focused )
        d->focused = nullptr;

    disconnect(child, &Widget::focus_changed, this, &WidgetContainer::focus_test);
    disconnect(child, &Widget::widget_destroyed, this, &WidgetContainer::remove);
}

void WidgetContainer::render(Renderer& params) const
{
    for ( auto w : d->children )
        w->render(params);
}

Intersection WidgetContainer::intersects(const Ray& ray) const
{
    Intersection best;
    best.ratio = 2;
    for ( auto widget : d->children )
    {
        if ( Intersection current = widget->intersects(ray) )
        {
            if ( current.ratio < best.ratio )
            {
                std::swap(best, current);
            }
        }
    }
    if ( best )
        best.push_parent(const_cast<WidgetContainer*>(this));
    return best;
}

void WidgetContainer::prepare_mouse_event(const Ray& ray, QMouseEvent* ev)
{
    Intersection intersection;

    // click select item to focus / activate
    if ( ev->type() == QEvent::MouseButtonPress || ev->type() == QEvent::MouseButtonDblClick )
    {
        // get intersection
        intersection = intersects(ray);
        // chev.intersection.widget should now be this or nullptr
        intersection.pop_parent();
        // chev.intersection.widget should now be in children() or nullptr
    }
    else if ( d->active )
    {
        // only consider the active item for intersections
        intersection = d->active->intersects(ray);
    }

    mouse_event({ray, ev, intersection});
}

void WidgetContainer::mouse_event(const MouseEvent& parent_ev)
{
    auto ev = parent_ev;
    if ( ev.intersection.widget == this )
        ev.intersection.pop_parent();

    if ( ev.type == QEvent::MouseButtonPress || ev.type == QEvent::MouseButtonDblClick )
    {
        d->active = ev.intersection.widget;
    }

    // Update focused child
    if ( d->focused != d->active && d->focused )
        d->focused->set_focus(false);
    d->focused = d->active;

    // chev.intersection.widget will have the active child
    Widget::mouse_event(ev);

    // propagate mouse event
    if ( d->active )
    {
        if ( d->active == this )
            return;
        d->active->mouse_event(ev);

        if ( ev.type == QEvent::MouseButtonRelease )
            d->active = nullptr;
    }
}

void WidgetContainer::on_focus(bool focused)
{
    if ( !focused )
    {
        for ( auto widget : d->children )
            widget->set_focus(false);
    }
}

const QVector<Widget*>& WidgetContainer::children() const
{
    return d->children;
}

void WidgetContainer::focus_test(bool focused)
{
    if ( focused )
    {
        set_focus(true);
        Widget* focused = qobject_cast<Widget*>(QObject::sender());
        if ( d->focused && focused != d->focused )
            d->focused->set_focus(false);
        d->focused = focused;
    }
}
