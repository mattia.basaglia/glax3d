#include "glax3d/collisions.h"

#include <cmath>
#include "glax3d/hecking_windoze.h"

using namespace glax3d::collisions;
using namespace glax3d;

int AxisAlignedBox::intersects(const Ray& ray, float* out_ratio) const
{
    float collision_min = -std::numeric_limits<float>::infinity();
    float collision_max = std::numeric_limits<float>::infinity();

    for ( int c = 0; c < 3; c++ ) // loop xyz
    {
        float t_min = (point1[c] - ray.start()[c]) / ray.vector_length()[c];
        float t_max = (point2[c] - ray.start()[c]) / ray.vector_length()[c];
        if ( t_min > t_max )
        {
            std::swap(t_min, t_max);
        }
        if ( t_min > collision_min )
            collision_min = t_min;
        if ( t_max < collision_max )
            collision_max = t_max;

        if ( c > 0  && (t_max < collision_min || t_min > collision_max) )
            return 0;
    }

    if ( out_ratio )
    {
        // collision_min < 0 if ray starts inside the box
        *out_ratio = collision_min < 0 ? collision_max : collision_min;
    }
    return 1;
}

bool AxisAlignedBox::intersects(const AxisAlignedBox& other) const
{
    auto c = (point1 + point2) / 2;
    auto co = (other.point1 + other.point2) / 2;
    auto sz = point2 - point1;
    auto szo = other.point2 - other.point1;

    for ( int i = 0; i < 3; i++ )
    {
        if ( std::abs(c[i] - co[i]) * 2 > sz[i] + szo[i] )
            return false;
    }

    return true;
}


int MultiCollision::intersects(const Ray& ray, float* ratio) const
{
    int best_priority = std::numeric_limits<int>::min();
    int best = 0;
    float shortest = 2;
    float current;
    for ( int i = 0; i < int(shapes.size()); i++ )
    {
        if ( shapes[i].second->intersects(ray, &current) )
        {
            if ( shapes[i].first > best_priority ||
                (shapes[i].first == best_priority && current < shortest) )
            {
                best_priority = shapes[i].first;
                shortest = current;
                best = i + 1;
            }
        }
    }
    if ( ratio && best )
        *ratio = shortest;
    return best;
}


namespace {

int sphere_intersects(const QVector3D& center, float radius, const Ray& ray, float* ratio)
{
    QVector3D ray2center(center - ray.start());

    // Project the sphere center to the ray (closest point in the ray to the center)
    float center_ray_proj = QVector3D::dotProduct(ray2center, ray.direction());

    // Use pythagoras ( v.v == ||v||^2 )
    float rad2 = radius * radius;
    float len_sq = ray2center.lengthSquared() - (center_ray_proj * center_ray_proj);
    if ( len_sq > rad2 )
        return 0;

    if ( ratio )
    {
        // Find the points from the closest point to the edge of the sphere
        float locr = std::sqrt(rad2 - len_sq);

        // if ray starts inside the sphere, return the far point
        float len = center_ray_proj - locr;
        if ( ray2center.lengthSquared() < rad2 )
            len = center_ray_proj + locr;

        // return the near point
        *ratio = len / ray.length();
    }

    return 1;
}

} // namespace

int Sphere::intersects(const Ray& ray, float* ratio) const
{
    return sphere_intersects(center, radius, ray, ratio);
}


int Capsule::intersects(const glax3d::Ray& ray, float* ratio) const
{

    // collision with infinite cylinder
    QVector3D ray_norm = ray.direction();
    QVector3D AB = b - a;                // capsule segment axis
    QVector3D AO = ray.start() - a;      // segment between ray start and capsule axis
    QVector3D AOxAB = QVector3D::crossProduct(AO, AB);      // normal to AO and AB
    QVector3D VxAB = QVector3D::crossProduct(ray_norm, AB); // normal between ray and axis
    // quadratic parameters
    float ab2 = AB.lengthSquared();// |AB|^2
    float q_a = VxAB.lengthSquared();// |VxAB|^2
    float q_b = 2 * QVector3D::dotProduct(VxAB, AOxAB);
    float q_c = AOxAB.lengthSquared() - radius * radius * ab2;
    float q_d = q_b * q_b - 4 * q_a * q_c;

    // ray misses the cylinder
    if ( q_d < 0 )
        return 0;

    float t = (-q_b - std::sqrt(q_d)) / (2 * q_a);
    // Goes through both caps (ie: does not hit the body of the cylinder
    if ( t < 0 )
    {
        const QVector3D& dome_center =
            (a - ray.start()).lengthSquared() < (b - ray.start()).lengthSquared()
            ? a : b
        ;
        return sphere_intersects(dome_center, radius, ray, ratio);
    }

    QVector3D cylhit = ray_norm * t + ray.start(); // Infinite cylinder intersection point
    float proj = QVector3D::dotProduct(cylhit - a, AB) / ab2;  // project the intersection point to the capsule axis
    if ( proj < 0 )
        return sphere_intersects(a, radius, ray, ratio);
    else if ( proj > 1 )
        return sphere_intersects(b, radius, ray, ratio);

    // intersection between the bounds of the cylinder proper
    if ( ratio )
        *ratio = t / ray.length();
    return 1;
}
