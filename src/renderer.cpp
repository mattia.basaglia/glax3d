#include "glax3d/renderer.h"

#include "glax3d/exception.h"
#include "glax3d/shape_buffer.h"
#include "glax3d/material.h"

using namespace glax3d;


void glax3d::Renderer::initialize()
{
    QOpenGLFunctions::initializeOpenGLFunctions();
}

void glax3d::Renderer::rendering_initialize()
{
    _transforms.clear();
    _transforms.push_back(QMatrix4x4());
}

QMatrix4x4 & glax3d::Renderer::transform()
{
    return _transforms.back();
}

QMatrix4x4 & glax3d::Renderer::push()
{
    _transforms.push_back(_transforms.back());
    return _transforms.back();
}

QMatrix4x4 & glax3d::Renderer::pop()
{
    if ( _transforms.size() < 2 )
        throw Glax3DError("Too many matrix pops");
    _transforms.pop_back();
    return _transforms.back();
}
