#include <QtTest/QtTest>

#include "glax3d/octree.h"

struct Point
{
    using scalar_type = float;

    QVector3D pos;
    QColor color;

    scalar_type x() const { return pos.x(); }
    scalar_type y() const { return pos.y(); }
    scalar_type z() const { return pos.z(); }

    Point(QVector3D pos, QColor color)
        : pos(pos), color(color)
    {}
};
using PointShapeBuffer = glax3d::ShapeBuffer<Point>;


class test_octree: public QObject
{
    Q_OBJECT

    using Octree = glax3d::Octree<Point, glax3d::VertexTraits<Point, PointShapeBuffer>>;
    using DataOctree = glax3d::Octree<Point, glax3d::VertexTraits<Point, PointShapeBuffer>, int>;

    static constexpr auto q3d = &Octree::traits_type::coord_vec;

private slots:

    void initTestCase()
    {
    }

    void test_ctor()
    {
        Octree ot;
        QVERIFY(ot.split_count() == 1024);
        QVERIFY(ot.min().x() < ot.max().x());
        QVERIFY(ot.min().y() < ot.max().y());
        QVERIFY(ot.min().z() < ot.max().z());
        QVERIFY(ot.max().x() - ot.min().x() >= ot.min_node_size());
        QVERIFY(ot.max().y() - ot.min().y() >= ot.min_node_size());
        QVERIFY(ot.max().z() - ot.min().z() >= ot.min_node_size());
    }

    void test_traits()
    {
        auto t = Octree::traits_type().coord(Point({1,2,3}, {4,5,6}));
        QVERIFY(t.x == 1);
        QVERIFY(t.y == 2);
        QVERIFY(t.z == 3);
    }

    void test_insert_nosplit()
    {
        Octree ot;
        PointShapeBuffer pb(GL_POINTS);
        Point p({1,2,3}, {4,5,6});

        for ( Octree::size_type i; i < ot.split_count(); i++ )
        {
            pb.push_back(p);
        }

        ot.add_indices(pb, 0, pb.size());
        QVERIFY(ot._top_node._size == pb.size());
        QVERIFY(ot.size() == pb.size());
        QVERIFY(ot._top_node.leaf());
        QVERIFY(ot._top_node._children.empty());


        auto v = ot._top_node._indices;
        QVERIFY(v.size() == pb.size());
        for ( Octree::size_type i; i < pb.size(); i++ )
        {
            QVERIFY(v[i] == i);
        }
    }

    void test_insert_split_to_min()
    {
        Octree ot;
        PointShapeBuffer pb(GL_POINTS);
        Point p({1,2,3}, {4,5,6});
        QVERIFY(ot.split_count() == 1024);

        for ( Octree::size_type i = 0; i <= ot.split_count(); i++ )
        {
            pb.push_back(p);
        }

        for ( Octree::size_type i = 0; i <= ot.split_count(); i++ )
            QCOMPARE(QVector3D(pb[i].pos), QVector3D(1,2,3));

        ot.add_indices(pb, 0, pb.size());
        QVERIFY(ot._top_node._size == pb.size());
        QVERIFY(ot.size() == pb.size());
        QVERIFY(!ot._top_node.leaf());
        QVERIFY(ot._top_node._children.size() == 8);
        QVERIFY(ot._top_node._children[ot._top_node.child_index(Octree::traits_type::coord(p))].size() == pb.size());


        Octree::value_type *node = &ot._top_node;
        while ( !node->leaf() )
        {
            node = &node->_children[node->child_index(Octree::traits_type::coord(p))];
        }

        auto v = node->_indices;
        QVERIFY(v.size() == pb.size());
        for ( Octree::size_type i; i < pb.size(); i++ )
        {
            QVERIFY(v[i] == i);
        }
        QVERIFY(node->_max.x - node->_min.x == ot.min_node_size());
        QVERIFY(node->_max.y - node->_min.y == ot.min_node_size());
        QVERIFY(node->_max.z - node->_min.z == ot.min_node_size());
    }

    void test_insert_split_location()
    {
        Octree ot(2.1, 0.1, 3);
        PointShapeBuffer pb(GL_POINTS);

        for ( int x = -1; x < 1; x++ )
            for ( int y = -1; y < 1; y++ )
                for ( int z = -1; z < 1; z++ )
                {
                    pb.push_back(Point(QVector3D(x+0.1, y+0.1, z+0.1), {4,5,6}));
                    pb.push_back(Point(QVector3D(x+0.2, y+0.2, z+0.2), {4,5,6}));
                }

        ot.add_indices(pb, 0, pb.size());

        QVERIFY(ot._top_node._size == 16);
        QVERIFY(ot.size() == 16);
        QVERIFY(!ot._top_node.leaf());
        QVERIFY(ot._top_node._children.size() == 8);

        {
            auto& c_xyz = ot._top_node._children[0];
            QCOMPARE(c_xyz.size(), 2);
            QVERIFY(c_xyz.leaf());
            QCOMPARE(QVector3D(pb[c_xyz._indices[0]].pos), QVector3D(-1+0.1, -1+0.1, -1+0.1));
            QCOMPARE(QVector3D(pb[c_xyz._indices[1]].pos), QVector3D(-1+0.2, -1+0.2, -1+0.2));
        }
        {
            auto& c_xyZ = ot._top_node._children[1];
            QCOMPARE(c_xyZ.size(), 2);
            QVERIFY(c_xyZ.leaf());
            QCOMPARE(QVector3D(pb[c_xyZ._indices[0]].pos), QVector3D(-1+0.1, -1+0.1, -0+0.1));
            QCOMPARE(QVector3D(pb[c_xyZ._indices[1]].pos), QVector3D(-1+0.2, -1+0.2, -0+0.2));
        }
        {
            auto& c_Xyz = ot._top_node._children[2];
            QVERIFY(c_Xyz.leaf());
            QCOMPARE(c_Xyz.size(), 2);
            QCOMPARE(QVector3D(pb[c_Xyz._indices[0]].pos), QVector3D(-0+0.1, -1+0.1, -1+0.1));
            QCOMPARE(QVector3D(pb[c_Xyz._indices[1]].pos), QVector3D(-0+0.2, -1+0.2, -1+0.2));
        }
        {
            auto& c_XyZ = ot._top_node._children[3];
            QCOMPARE(c_XyZ.size(), 2);
            QVERIFY(c_XyZ.leaf());
            QCOMPARE(QVector3D(pb[c_XyZ._indices[0]].pos), QVector3D(-0+0.1, -1+0.1, -0+0.1));
            QCOMPARE(QVector3D(pb[c_XyZ._indices[1]].pos), QVector3D(-0+0.2, -1+0.2, -0+0.2));
        }
        {
            auto& c_xYz = ot._top_node._children[4];
            QVERIFY(c_xYz.leaf());
            QVERIFY(c_xYz.size() == 2);
            QCOMPARE(QVector3D(pb[c_xYz._indices[0]].pos), QVector3D(-1+0.1, -0+0.1, -1+0.1));
            QCOMPARE(QVector3D(pb[c_xYz._indices[1]].pos), QVector3D(-1+0.2, -0+0.2, -1+0.2));
        }
        {
            auto& c_xYZ = ot._top_node._children[5];
            QVERIFY(c_xYZ.leaf());
            QVERIFY(c_xYZ.size() == 2);
            QCOMPARE(QVector3D(pb[c_xYZ._indices[0]].pos), QVector3D(-1+0.1, -0+0.1, -0+0.1));
            QCOMPARE(QVector3D(pb[c_xYZ._indices[1]].pos), QVector3D(-1+0.2, -0+0.2, -0+0.2));
        }
        {
            auto& c_XYz = ot._top_node._children[6];
            QVERIFY(c_XYz.leaf());
            QVERIFY(c_XYz.size() == 2);
            QCOMPARE(QVector3D(pb[c_XYz._indices[0]].pos), QVector3D(-0+0.1, -0+0.1, -1+0.1));
            QCOMPARE(QVector3D(pb[c_XYz._indices[1]].pos), QVector3D(-0+0.2, -0+0.2, -1+0.2));
        }
        {
            auto& c_XYZ = ot._top_node._children[7];
            QCOMPARE(c_XYZ.size(), 2);
            QVERIFY(c_XYZ.leaf());
            QCOMPARE(QVector3D(pb[c_XYZ._indices[0]].pos), QVector3D(-0+0.1, -0+0.1, -0+0.1));
            QCOMPARE(QVector3D(pb[c_XYZ._indices[1]].pos), QVector3D(-0+0.2, -0+0.2, -0+0.2));
        }
    }

    void test_octree_expand_index()
    {
        Octree ot(2, 0);
        QCOMPARE(ot._top_node.expand_direction({+2, +2, +2}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({+0, +0, -2}).second, 1);
        QCOMPARE(ot._top_node.expand_direction({-2, +0, +0}).second, 2);
        QCOMPARE(ot._top_node.expand_direction({-2, +0, -2}).second, 3);
        QCOMPARE(ot._top_node.expand_direction({+0, -2, +0}).second, 4);
        QCOMPARE(ot._top_node.expand_direction({+0, -2, -2}).second, 5);
        QCOMPARE(ot._top_node.expand_direction({-2, -2, +0}).second, 6);
        QCOMPARE(ot._top_node.expand_direction({-2, -2, -2}).second, 7);

        QCOMPARE(ot._top_node.expand_direction({-2, +0, +2}).second, 2);
        QCOMPARE(ot._top_node.expand_direction({+0, +0, +2}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({+2, +0, +2}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({+2, +0, +0}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({+2, +0, -2}).second, 1);

        QCOMPARE(ot._top_node.expand_direction({-2, -2, +2}).second, 6);
        QCOMPARE(ot._top_node.expand_direction({+0, -2, +2}).second, 4);
        QCOMPARE(ot._top_node.expand_direction({+2, -2, +2}).second, 4);
        QCOMPARE(ot._top_node.expand_direction({+2, -2, +0}).second, 4);
        QCOMPARE(ot._top_node.expand_direction({+2, -2, -2}).second, 5);

        QCOMPARE(ot._top_node.expand_direction({+0, +2, +0}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({+0, +2, +2}).second, 0);
        QCOMPARE(ot._top_node.expand_direction({-2, +2, +2}).second, 2);
        QCOMPARE(ot._top_node.expand_direction({-2, +2, +0}).second, 2);
        QCOMPARE(ot._top_node.expand_direction({-2, +2, -2}).second, 3);
        QCOMPARE(ot._top_node.expand_direction({+0, +2, -2}).second, 1);
        QCOMPARE(ot._top_node.expand_direction({+2, +2, -2}).second, 1);
        QCOMPARE(ot._top_node.expand_direction({+2, +2, +0}).second, 0);
    }

    void test_octree_expand_mid()
    {
        Octree ot(2, 0);
        auto& tn = ot._top_node;
        Octree::coord_type c[8] = {
            {tn._max.x, tn._max.y, tn._max.z},
            {tn._max.x, tn._max.y, tn._min.z},
            {tn._min.x, tn._max.y, tn._max.z},
            {tn._min.x, tn._max.y, tn._min.z},

            {tn._max.x, tn._min.y, tn._max.z},
            {tn._max.x, tn._min.y, tn._min.z},
            {tn._min.x, tn._min.y, tn._max.z},
            {tn._min.x, tn._min.y, tn._min.z}
        };

        QCOMPARE(q3d(tn.expand_direction({+2, +2, +2}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({+0, +0, -2}).first), q3d(c[1]));
        QCOMPARE(q3d(tn.expand_direction({-2, +0, +0}).first), q3d(c[2]));
        QCOMPARE(q3d(tn.expand_direction({-2, +0, -2}).first), q3d(c[3]));
        QCOMPARE(q3d(tn.expand_direction({+0, -2, +0}).first), q3d(c[4]));
        QCOMPARE(q3d(tn.expand_direction({+0, -2, -2}).first), q3d(c[5]));
        QCOMPARE(q3d(tn.expand_direction({-2, -2, +0}).first), q3d(c[6]));
        QCOMPARE(q3d(tn.expand_direction({-2, -2, -2}).first), q3d(c[7]));

        QCOMPARE(q3d(tn.expand_direction({-2, +0, +2}).first), q3d(c[2]));
        QCOMPARE(q3d(tn.expand_direction({+0, +0, +2}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({+2, +0, +2}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({+2, +0, +0}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({+2, +0, -2}).first), q3d(c[1]));

        QCOMPARE(q3d(tn.expand_direction({-2, -2, +2}).first), q3d(c[6]));
        QCOMPARE(q3d(tn.expand_direction({+0, -2, +2}).first), q3d(c[4]));
        QCOMPARE(q3d(tn.expand_direction({+2, -2, +2}).first), q3d(c[4]));
        QCOMPARE(q3d(tn.expand_direction({+2, -2, +0}).first), q3d(c[4]));
        QCOMPARE(q3d(tn.expand_direction({+2, -2, -2}).first), q3d(c[5]));

        QCOMPARE(q3d(tn.expand_direction({+0, +2, +0}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({+0, +2, +2}).first), q3d(c[0]));
        QCOMPARE(q3d(tn.expand_direction({-2, +2, +2}).first), q3d(c[2]));
        QCOMPARE(q3d(tn.expand_direction({-2, +2, +0}).first), q3d(c[2]));
        QCOMPARE(q3d(tn.expand_direction({-2, +2, -2}).first), q3d(c[3]));
        QCOMPARE(q3d(tn.expand_direction({+0, +2, -2}).first), q3d(c[1]));
        QCOMPARE(q3d(tn.expand_direction({+2, +2, -2}).first), q3d(c[1]));
        QCOMPARE(q3d(tn.expand_direction({+2, +2, +0}).first), q3d(c[0]));
    }

    void test_octree_in_bounds_out()
    {
        Octree ot(2, 0);

        QVERIFY(!ot._top_node.in_bounds({+2, +2, +2}));
        QVERIFY(!ot._top_node.in_bounds({+0, +0, -2}));
        QVERIFY(!ot._top_node.in_bounds({-2, +0, +0}));
        QVERIFY(!ot._top_node.in_bounds({-2, +0, -2}));
        QVERIFY(!ot._top_node.in_bounds({+0, -2, +0}));
        QVERIFY(!ot._top_node.in_bounds({+0, -2, -2}));
        QVERIFY(!ot._top_node.in_bounds({-2, -2, +0}));
        QVERIFY(!ot._top_node.in_bounds({-2, -2, -2}));

        QVERIFY(!ot._top_node.in_bounds({-2, +0, +2}));
        QVERIFY(!ot._top_node.in_bounds({+0, +0, +2}));
        QVERIFY(!ot._top_node.in_bounds({+2, +0, +2}));
        QVERIFY(!ot._top_node.in_bounds({+2, +0, +0}));
        QVERIFY(!ot._top_node.in_bounds({+2, +0, -2}));

        QVERIFY(!ot._top_node.in_bounds({-2, -2, +2}));
        QVERIFY(!ot._top_node.in_bounds({+0, -2, +2}));
        QVERIFY(!ot._top_node.in_bounds({+2, -2, +2}));
        QVERIFY(!ot._top_node.in_bounds({+2, -2, +0}));
        QVERIFY(!ot._top_node.in_bounds({+2, -2, -2}));

        QVERIFY(!ot._top_node.in_bounds({+0, +2, +0}));
        QVERIFY(!ot._top_node.in_bounds({+0, +2, +2}));
        QVERIFY(!ot._top_node.in_bounds({-2, +2, +2}));
        QVERIFY(!ot._top_node.in_bounds({-2, +2, +0}));
        QVERIFY(!ot._top_node.in_bounds({-2, +2, -2}));
        QVERIFY(!ot._top_node.in_bounds({+0, +2, -2}));
        QVERIFY(!ot._top_node.in_bounds({+2, +2, -2}));
        QVERIFY(!ot._top_node.in_bounds({+2, +2, +0}));
    }

    void test_expand_single()
    {
        Octree ot(2, 0);
        PointShapeBuffer pb(GL_POINTS);
        pb.push_back(Point{{0, 0, 0}, {4, 5, 6}});
        pb.push_back(Point{{2, 2, 2}, {4, 5, 6}});

        ot.add_indices(pb, 0, 2);
        QCOMPARE(ot.size(), 2);
        QVERIFY(!ot._top_node.leaf());
        QCOMPARE(ot._top_node._children[0].size(), 1);
        QCOMPARE(ot._top_node._children[0]._indices[0], 0);
        QCOMPARE(ot._top_node._children[1].size(), 0);
        QCOMPARE(ot._top_node._children[2].size(), 0);
        QCOMPARE(ot._top_node._children[3].size(), 0);
        QCOMPARE(ot._top_node._children[4].size(), 0);
        QCOMPARE(ot._top_node._children[5].size(), 0);
        QCOMPARE(ot._top_node._children[6].size(), 0);
        QCOMPARE(ot._top_node._children[7].size(), 1);
        QCOMPARE(ot._top_node._children[7]._indices[0], 1);
        QCOMPARE(q3d(ot._top_node._min), QVector3D(-1, -1, -1));
        QCOMPARE(q3d(ot._top_node._max), QVector3D(3, 3, 3));
        QCOMPARE(q3d(ot._top_node._min), q3d(ot._top_node._children[0]._min));
        QCOMPARE(q3d(ot._top_node._mid), q3d(ot._top_node._children[0]._max));
    }

    void test_expand_multi()
    {
        Octree ot(2, 0);
        PointShapeBuffer pb(GL_POINTS);
        pb.push_back(Point{{0, 0, 0}, {4, 5, 6}});
        pb.push_back(Point{{3, 3, 3}, {4, 5, 6}});

        ot.add_indices(pb, 0, 2);
        QCOMPARE(ot.size(), 2);
        QVERIFY(!ot._top_node.leaf());
        QCOMPARE(ot._top_node._children[0]._children[0].size(), 1);
        QCOMPARE(ot._top_node._children[0]._children[0]._indices[0], 0);
        QCOMPARE(ot._top_node._children[7].size(), 1);
        QCOMPARE(ot._top_node._children[7]._indices[0], 1);
        QCOMPARE(q3d(ot._top_node._min), QVector3D(-1, -1, -1));
        QCOMPARE(q3d(ot._top_node._mid), QVector3D(3, 3, 3));
        QCOMPARE(q3d(ot._top_node._max), QVector3D(7, 7, 7));
    }

    void test_erase_leaf()
    {
        Octree ot;
        PointShapeBuffer pb1(GL_POINTS);
        Point p{{0, 0, 0}, {4, 5, 6}};
        for ( int i = 0; i < 20; i++ )
        {
            pb1.push_back(p);
        }

        ot.add_indices(pb1, 0, 20);

        QCOMPARE(ot.size(), 20);
        ot.clear();
        QCOMPARE(ot.size(), 0);
    }

    void test_erase_inner()
    {
        Octree ot;
        ot._merge_count = 12;
        ot._top_node.split_create_children();
        PointShapeBuffer pb1(GL_POINTS);
        Point p{{0, 0, 0}, {4, 5, 6}};
        for ( int i = 0; i < 30; i++ )
        {
            pb1.push_back(p);
        }

        ot.add_indices(pb1, 0, 30);
        QCOMPARE(ot.size(), 30);
        QVERIFY(!ot._top_node.leaf());
        ot.clear();
        QCOMPARE(ot.size(), 0);
        QVERIFY(ot._top_node.leaf());
    }

    void test_leaf_iterator()
    {
        Octree ot;
        ot._top_node.split_create_children();
        ot._top_node._children[0].split_create_children();
        ot._top_node._children[0]._children[1].split_create_children();
        ot._top_node._children[7].split_create_children();
        using node = Octree::value_type;

        node* top = &ot._top_node;     // parent

        node* n0 = &top->_children[0]; // parent
        node* n1 = &top->_children[1];
        node* n2 = &top->_children[2];
        node* n3 = &top->_children[3];
        node* n4 = &top->_children[4];
        node* n5 = &top->_children[5];
        node* n6 = &top->_children[6];
        node* n7 = &top->_children[7]; // parent

        node* n00 = &n0->_children[0];
        node* n01 = &n0->_children[1]; // parent
        node* n02 = &n0->_children[2];
        node* n03 = &n0->_children[3];
        node* n04 = &n0->_children[4];
        node* n05 = &n0->_children[5];
        node* n06 = &n0->_children[6];
        node* n07 = &n0->_children[7];

        node* n010 = &n01->_children[0];
        node* n011 = &n01->_children[1];
        node* n012 = &n01->_children[2];
        node* n013 = &n01->_children[3];
        node* n014 = &n01->_children[4];
        node* n015 = &n01->_children[5];
        node* n016 = &n01->_children[6];
        node* n017 = &n01->_children[7];

        node* n70 = &n7->_children[0];
        node* n71 = &n7->_children[1];
        node* n72 = &n7->_children[2];
        node* n73 = &n7->_children[3];
        node* n74 = &n7->_children[4];
        node* n75 = &n7->_children[5];
        node* n76 = &n7->_children[6];
        node* n77 = &n7->_children[7];

        Octree::iterator iterator;
        iterator.leaves_only = true;
        iterator.stack.push_back(top);
        QCOMPARE(iterator.node(), top);
        iterator.step(); QCOMPARE(iterator.node(), n0);
        iterator.step(); QCOMPARE(iterator.node(), n00);
        iterator.step(); QCOMPARE(iterator.node(), n01);
        iterator.step(); QCOMPARE(iterator.node(), n010);
        iterator.step(); QCOMPARE(iterator.node(), n011);
        iterator.step(); QCOMPARE(iterator.node(), n012);
        iterator.step(); QCOMPARE(iterator.node(), n013);
        iterator.step(); QCOMPARE(iterator.node(), n014);
        iterator.step(); QCOMPARE(iterator.node(), n015);
        iterator.step(); QCOMPARE(iterator.node(), n016);
        iterator.step(); QCOMPARE(iterator.node(), n017);
        iterator.step(); QCOMPARE(iterator.node(), n02);
        iterator.step(); QCOMPARE(iterator.node(), n03);
        iterator.step(); QCOMPARE(iterator.node(), n04);
        iterator.step(); QCOMPARE(iterator.node(), n05);
        iterator.step(); QCOMPARE(iterator.node(), n06);
        iterator.step(); QCOMPARE(iterator.node(), n07);
        iterator.step(); QCOMPARE(iterator.node(), n1);
        iterator.step(); QCOMPARE(iterator.node(), n2);
        iterator.step(); QCOMPARE(iterator.node(), n3);
        iterator.step(); QCOMPARE(iterator.node(), n4);
        iterator.step(); QCOMPARE(iterator.node(), n5);
        iterator.step(); QCOMPARE(iterator.node(), n6);
        iterator.step(); QCOMPARE(iterator.node(), n7);
        iterator.step(); QCOMPARE(iterator.node(), n70);
        iterator.step(); QCOMPARE(iterator.node(), n71);
        iterator.step(); QCOMPARE(iterator.node(), n72);
        iterator.step(); QCOMPARE(iterator.node(), n73);
        iterator.step(); QCOMPARE(iterator.node(), n74);
        iterator.step(); QCOMPARE(iterator.node(), n75);
        iterator.step(); QCOMPARE(iterator.node(), n76);
        iterator.step(); QCOMPARE(iterator.node(), n77);
        iterator.step(); QVERIFY(iterator.end());

        iterator = ot.leaves().begin();
        QCOMPARE(iterator.node(), n00);
        ++iterator; QCOMPARE(iterator.node(), n010);
        ++iterator; QCOMPARE(iterator.node(), n011);
        ++iterator; QCOMPARE(iterator.node(), n012);
        ++iterator; QCOMPARE(iterator.node(), n013);
        ++iterator; QCOMPARE(iterator.node(), n014);
        ++iterator; QCOMPARE(iterator.node(), n015);
        ++iterator; QCOMPARE(iterator.node(), n016);
        ++iterator; QCOMPARE(iterator.node(), n017);
        ++iterator; QCOMPARE(iterator.node(), n02);
        ++iterator; QCOMPARE(iterator.node(), n03);
        ++iterator; QCOMPARE(iterator.node(), n04);
        ++iterator; QCOMPARE(iterator.node(), n05);
        ++iterator; QCOMPARE(iterator.node(), n06);
        ++iterator; QCOMPARE(iterator.node(), n07);
        ++iterator; QCOMPARE(iterator.node(), n1);
        ++iterator; QCOMPARE(iterator.node(), n2);
        ++iterator; QCOMPARE(iterator.node(), n3);
        ++iterator; QCOMPARE(iterator.node(), n4);
        ++iterator; QCOMPARE(iterator.node(), n5);
        ++iterator; QCOMPARE(iterator.node(), n6);
        ++iterator; QCOMPARE(iterator.node(), n70);
        ++iterator; QCOMPARE(iterator.node(), n71);
        ++iterator; QCOMPARE(iterator.node(), n72);
        ++iterator; QCOMPARE(iterator.node(), n73);
        ++iterator; QCOMPARE(iterator.node(), n74);
        ++iterator; QCOMPARE(iterator.node(), n75);
        ++iterator; QCOMPARE(iterator.node(), n76);
        ++iterator; QCOMPARE(iterator.node(), n77);
        ++iterator; QVERIFY(iterator.end());
        QVERIFY(iterator == ot.end());

        Octree::iterator iterator1 = ot.leaves().begin();
        iterator = ot.leaves().begin();
        QVERIFY(iterator == iterator1++);
        QVERIFY(++iterator == iterator1);
        QVERIFY(iterator == iterator1);
        ++iterator;
        QVERIFY(iterator != iterator1);

        for ( iterator = ot.leaves().begin(); iterator != ot.leaves().end(); ++iterator )
        {
            QVERIFY(&(*iterator) == iterator.operator->());
            QVERIFY(&(*iterator) == iterator.node());
        }

        iterator = ot.leaves().begin();
        QCOMPARE(iterator.node(), n00);
        QCOMPARE(iterator->min(), q3d(n00->_min));
        QCOMPARE(iterator->max(), q3d(n00->_max));
        QCOMPARE(iterator->center(), q3d(n00->_mid));
        QCOMPARE(iterator->size(), n00->_size);
        QCOMPARE(iterator->box().point1, q3d(n00->_min));
        QCOMPARE(iterator->box().point2, q3d(n00->_max));
    }

    void test_full_iterator()
    {
        Octree ot;
        ot._top_node.split_create_children();
        ot._top_node._children[0].split_create_children();
        ot._top_node._children[0]._children[1].split_create_children();
        ot._top_node._children[7].split_create_children();
        using node = Octree::value_type;

        node* top = &ot._top_node;     // parent

        node* n0 = &top->_children[0]; // parent
        node* n1 = &top->_children[1];
        node* n2 = &top->_children[2];
        node* n3 = &top->_children[3];
        node* n4 = &top->_children[4];
        node* n5 = &top->_children[5];
        node* n6 = &top->_children[6];
        node* n7 = &top->_children[7]; // parent

        node* n00 = &n0->_children[0];
        node* n01 = &n0->_children[1]; // parent
        node* n02 = &n0->_children[2];
        node* n03 = &n0->_children[3];
        node* n04 = &n0->_children[4];
        node* n05 = &n0->_children[5];
        node* n06 = &n0->_children[6];
        node* n07 = &n0->_children[7];

        node* n010 = &n01->_children[0];
        node* n011 = &n01->_children[1];
        node* n012 = &n01->_children[2];
        node* n013 = &n01->_children[3];
        node* n014 = &n01->_children[4];
        node* n015 = &n01->_children[5];
        node* n016 = &n01->_children[6];
        node* n017 = &n01->_children[7];

        node* n70 = &n7->_children[0];
        node* n71 = &n7->_children[1];
        node* n72 = &n7->_children[2];
        node* n73 = &n7->_children[3];
        node* n74 = &n7->_children[4];
        node* n75 = &n7->_children[5];
        node* n76 = &n7->_children[6];
        node* n77 = &n7->_children[7];

        Octree::iterator iterator;
        iterator.stack.push_back(top);
        QCOMPARE(iterator.node(), top);
        iterator.step(); QCOMPARE(iterator.node(), n0);
        iterator.step(); QCOMPARE(iterator.node(), n00);
        iterator.step(); QCOMPARE(iterator.node(), n01);
        iterator.step(); QCOMPARE(iterator.node(), n010);
        iterator.step(); QCOMPARE(iterator.node(), n011);
        iterator.step(); QCOMPARE(iterator.node(), n012);
        iterator.step(); QCOMPARE(iterator.node(), n013);
        iterator.step(); QCOMPARE(iterator.node(), n014);
        iterator.step(); QCOMPARE(iterator.node(), n015);
        iterator.step(); QCOMPARE(iterator.node(), n016);
        iterator.step(); QCOMPARE(iterator.node(), n017);
        iterator.step(); QCOMPARE(iterator.node(), n02);
        iterator.step(); QCOMPARE(iterator.node(), n03);
        iterator.step(); QCOMPARE(iterator.node(), n04);
        iterator.step(); QCOMPARE(iterator.node(), n05);
        iterator.step(); QCOMPARE(iterator.node(), n06);
        iterator.step(); QCOMPARE(iterator.node(), n07);
        iterator.step(); QCOMPARE(iterator.node(), n1);
        iterator.step(); QCOMPARE(iterator.node(), n2);
        iterator.step(); QCOMPARE(iterator.node(), n3);
        iterator.step(); QCOMPARE(iterator.node(), n4);
        iterator.step(); QCOMPARE(iterator.node(), n5);
        iterator.step(); QCOMPARE(iterator.node(), n6);
        iterator.step(); QCOMPARE(iterator.node(), n7);
        iterator.step(); QCOMPARE(iterator.node(), n70);
        iterator.step(); QCOMPARE(iterator.node(), n71);
        iterator.step(); QCOMPARE(iterator.node(), n72);
        iterator.step(); QCOMPARE(iterator.node(), n73);
        iterator.step(); QCOMPARE(iterator.node(), n74);
        iterator.step(); QCOMPARE(iterator.node(), n75);
        iterator.step(); QCOMPARE(iterator.node(), n76);
        iterator.step(); QCOMPARE(iterator.node(), n77);
        iterator.step(); QVERIFY(iterator.end());

        iterator = ot.begin();
        QCOMPARE(iterator.node(), top);
        ++iterator; QCOMPARE(iterator.node(), n0);
        ++iterator; QCOMPARE(iterator.node(), n00);
        ++iterator; QCOMPARE(iterator.node(), n01);
        ++iterator; QCOMPARE(iterator.node(), n010);
        ++iterator; QCOMPARE(iterator.node(), n011);
        ++iterator; QCOMPARE(iterator.node(), n012);
        ++iterator; QCOMPARE(iterator.node(), n013);
        ++iterator; QCOMPARE(iterator.node(), n014);
        ++iterator; QCOMPARE(iterator.node(), n015);
        ++iterator; QCOMPARE(iterator.node(), n016);
        ++iterator; QCOMPARE(iterator.node(), n017);
        ++iterator; QCOMPARE(iterator.node(), n02);
        ++iterator; QCOMPARE(iterator.node(), n03);
        ++iterator; QCOMPARE(iterator.node(), n04);
        ++iterator; QCOMPARE(iterator.node(), n05);
        ++iterator; QCOMPARE(iterator.node(), n06);
        ++iterator; QCOMPARE(iterator.node(), n07);
        ++iterator; QCOMPARE(iterator.node(), n1);
        ++iterator; QCOMPARE(iterator.node(), n2);
        ++iterator; QCOMPARE(iterator.node(), n3);
        ++iterator; QCOMPARE(iterator.node(), n4);
        ++iterator; QCOMPARE(iterator.node(), n5);
        ++iterator; QCOMPARE(iterator.node(), n6);
        ++iterator; QCOMPARE(iterator.node(), n7);
        ++iterator; QCOMPARE(iterator.node(), n70);
        ++iterator; QCOMPARE(iterator.node(), n71);
        ++iterator; QCOMPARE(iterator.node(), n72);
        ++iterator; QCOMPARE(iterator.node(), n73);
        ++iterator; QCOMPARE(iterator.node(), n74);
        ++iterator; QCOMPARE(iterator.node(), n75);
        ++iterator; QCOMPARE(iterator.node(), n76);
        ++iterator; QCOMPARE(iterator.node(), n77);
        ++iterator; QVERIFY(iterator.end());
        QVERIFY(iterator == ot.end());

        Octree::iterator iterator1 = ot.begin();
        iterator = ot.begin();
        QVERIFY(iterator == iterator1++);
        QVERIFY(++iterator == iterator1);
        QVERIFY(iterator == iterator1);
        ++iterator;
        QVERIFY(iterator != iterator1);

        for ( iterator = ot.begin(); iterator != ot.end(); ++iterator )
        {
            QVERIFY(&(*iterator) == iterator.operator->());
            QVERIFY(&(*iterator) == iterator.node());
        }

        iterator = ot.begin();
        QCOMPARE(iterator.node(), top);
        QCOMPARE(iterator->min(), q3d(top->_min));
        QCOMPARE(iterator->max(), q3d(top->_max));
        QCOMPARE(iterator->center(), q3d(top->_mid));
        QCOMPARE(iterator->size(), top->_size);
        QCOMPARE(iterator->box().point1, q3d(top->_min));
        QCOMPARE(iterator->box().point2, q3d(top->_max));
    }

    void test_octree_data_init()
    {
        DataOctree ot;
        ot._top_node.split_create_children();
        ot._top_node._children[0].split_create_children();
        ot._top_node._children[0]._children[1].split_create_children();
        ot._top_node._children[7].split_create_children();
        using node = DataOctree::value_type;

        int total = 1 + 8 * 4;
        int count = 0;
        for ( const node& n : ot )
        {
            QCOMPARE(n.get_data(), 0);
            count++;
        }

        QCOMPARE(count, total);
    }

    void test_octree_data_update()
    {
        DataOctree ot;
        ot._top_node.split_create_children();
        ot._top_node._children[0].split_create_children();
        ot._top_node._children[0]._children[1].split_create_children();
        ot._top_node._children[7].split_create_children();
        using node = DataOctree::value_type;
        auto func = [](node& n){
            if ( n.leaf() )
            {
                n.set_data(1);
            }
            else
            {
                for ( const auto& c : n.children() )
                    n.data() += c.get_data();
            }
        };

        node* top = &ot._top_node;     // parent

        node* n0 = &top->_children[0]; // parent
        node* n1 = &top->_children[1];
        node* n2 = &top->_children[2];
        node* n3 = &top->_children[3];
        node* n4 = &top->_children[4];
        node* n5 = &top->_children[5];
        node* n6 = &top->_children[6];
        node* n7 = &top->_children[7]; // parent

        node* n00 = &n0->_children[0];
        node* n01 = &n0->_children[1]; // parent
        node* n02 = &n0->_children[2];
        node* n03 = &n0->_children[3];
        node* n04 = &n0->_children[4];
        node* n05 = &n0->_children[5];
        node* n06 = &n0->_children[6];
        node* n07 = &n0->_children[7];

        node* n010 = &n01->_children[0];
        node* n011 = &n01->_children[1];
        node* n012 = &n01->_children[2];
        node* n013 = &n01->_children[3];
        node* n014 = &n01->_children[4];
        node* n015 = &n01->_children[5];
        node* n016 = &n01->_children[6];
        node* n017 = &n01->_children[7];

        node* n70 = &n7->_children[0];
        node* n71 = &n7->_children[1];
        node* n72 = &n7->_children[2];
        node* n73 = &n7->_children[3];
        node* n74 = &n7->_children[4];
        node* n75 = &n7->_children[5];
        node* n76 = &n7->_children[6];
        node* n77 = &n7->_children[7];

        ot.update_node_data(func);
        QCOMPARE(top->get_data(), 29);

        QCOMPARE(n0->get_data(), 15);
        QCOMPARE(n1->get_data(), 1);
        QCOMPARE(n2->get_data(), 1);
        QCOMPARE(n3->get_data(), 1);
        QCOMPARE(n4->get_data(), 1);
        QCOMPARE(n5->get_data(), 1);
        QCOMPARE(n6->get_data(), 1);
        QCOMPARE(n7->get_data(), 8);

        QCOMPARE(n00->get_data(), 1);
        QCOMPARE(n01->get_data(), 8);
        QCOMPARE(n02->get_data(), 1);
        QCOMPARE(n03->get_data(), 1);
        QCOMPARE(n04->get_data(), 1);
        QCOMPARE(n05->get_data(), 1);
        QCOMPARE(n06->get_data(), 1);
        QCOMPARE(n07->get_data(), 1);

        QCOMPARE(n010->get_data(), 1);
        QCOMPARE(n011->get_data(), 1);
        QCOMPARE(n012->get_data(), 1);
        QCOMPARE(n013->get_data(), 1);
        QCOMPARE(n014->get_data(), 1);
        QCOMPARE(n015->get_data(), 1);
        QCOMPARE(n016->get_data(), 1);
        QCOMPARE(n017->get_data(), 1);

        QCOMPARE(n70->get_data(), 1);
        QCOMPARE(n71->get_data(), 1);
        QCOMPARE(n72->get_data(), 1);
        QCOMPARE(n73->get_data(), 1);
        QCOMPARE(n74->get_data(), 1);
        QCOMPARE(n75->get_data(), 1);
        QCOMPARE(n76->get_data(), 1);
        QCOMPARE(n77->get_data(), 1);
    }


    void test_remove_leaf()
    {
        Octree ot;
        PointShapeBuffer pb1(GL_POINTS);
        Point p{{0, 0, 0}, {4, 5, 6}};
        for ( int i = 0; i < 20; i++ )
        {
            pb1.push_back(p);
        }

        ot.add_indices(pb1, 0, 20);

        QCOMPARE(ot.size(), 20);
        ot.remove_indices(0, 5);
        QCOMPARE(ot.size(), 15);
        ot.remove_indices(5, 10);
        QCOMPARE(ot.size(), 10);
        ot.remove_indices(0, 10);
        QCOMPARE(ot.size(), 10);
        ot.remove_indices(10, 15);
        QCOMPARE(ot.size(), 5);
        ot.remove_indices(15, 20);
        QCOMPARE(ot.size(), 0);
    }

    void test_remove_inner()
    {
        Octree ot;
        ot._merge_count = 12;
        ot._top_node.split_create_children();
        PointShapeBuffer pb1(GL_POINTS);
        Point p{{0, 0, 0}, {4, 5, 6}};
        for ( int i = 0; i < 40; i++ )
        {
            pb1.push_back(p);
        }

        ot.add_indices(pb1, 0, 25);

        QVERIFY(!ot._top_node.leaf());
        QCOMPARE(ot.size(), 25);
        ot.remove_indices(20, 30);
        QCOMPARE(ot.size(), 20);
        ot.remove_indices(30, 40);
        QCOMPARE(ot.size(), 20);
        ot.remove_indices(0, 5);
        QVERIFY(!ot._top_node.leaf());
        QCOMPARE(ot.size(), 15);
        ot.remove_indices(15, 20);
        QVERIFY(ot._top_node.leaf());
        QCOMPARE(ot.size(), 10);
        ot.remove_indices(10, 15);
        QVERIFY(ot._top_node.leaf());
        QCOMPARE(ot.size(), 5);
        ot.remove_indices(5, 10);
        QVERIFY(ot._top_node.leaf());
        QCOMPARE(ot.size(), 0);
    }

    void test_deep_copy_inner()
    {
        Octree ot1(2.1, 0.1, 3);
        PointShapeBuffer pb(GL_POINTS);

        for ( int x = -1; x < 1; x++ )
            for ( int y = -1; y < 1; y++ )
                for ( int z = -1; z < 1; z++ )
                {
                    pb.push_back(Point(QVector3D(x+0.1, y+0.1, z+0.1), {4,5,6}));
                    pb.push_back(Point(QVector3D(x+0.2, y+0.2, z+0.2), {4,5,6}));
                }

        ot1.add_indices(pb, 0, pb.size());

        QCOMPARE(ot1.size(), 16);
        QVERIFY(!ot1._top_node.leaf());

        Octree ot2 = ot1.deep_copy();
        QCOMPARE(ot2.size(), 16);
        QVERIFY(!ot2._top_node.leaf());
        for ( auto it1 = ot1.begin(), it2 = ot2.begin(); it1 != ot1.end(); ++it1, ++it2 )
        {
            QCOMPARE(it1->size(), it2->size());
            QCOMPARE(it1->leaf(), it2->leaf());
            for ( size_t i = 0; i < it1->_indices.size(); i++ )
            {
                QCOMPARE(it1->_indices[i], it2->_indices[i]);
            }
        }

        pb.push_back(Point(QVector3D(-1, -1, -1), {4,5,6}));
        ot1.add_indices(pb, pb.size()-1, pb.size());
        QCOMPARE(ot1.size(), 17);
        QCOMPARE(ot2.size(), 16);
        QCOMPARE(ot1._top_node._children[0].size(), 3);
        QCOMPARE(ot2._top_node._children[0].size(), 2);

    }
};


QTEST_GUILESS_MAIN(test_octree)
#include "test_octree.moc"
