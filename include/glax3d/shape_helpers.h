#ifndef GLAX3D_SHAPE_HELPERS_H
#define GLAX3D_SHAPE_HELPERS_H

#include <vector>
#include <cmath>
#include <QVector3D>

namespace glax3d {

/**
 * \brief Return an orthonormal vector of another vector
 */
inline QVector3D vector_normal(const QVector3D& v)
{
    if ( v.x() == 0 )
        return {1, 0, 0};

    if ( v.y() == 0 )
        return {0, 1, 0};

    if ( v.z() == 0 )
        return {0, 0, 1};

    return QVector3D::crossProduct(v, {1, 0, 1}).normalized();
}

/**
 * \brief Normalized slice of a cylinder between two points
 * \param a         Center of one of the two circular faces of the cylinder
 * \param b         Center of the other face
 * \param radius    Radius of the cylinder
 * \param n_segs    Number of segments for the circle
 * \returns A list of coordinates for a circle with the right orientation centered at 0
 */
inline std::vector<QVector3D> cylinder_slice(const QVector3D& a, const QVector3D& b, float radius, int n_segs)
{
    QVector3D v = b-a;
    QVector3D base1 = vector_normal(v);
    QVector3D base2 = QVector3D::crossProduct(v, base1).normalized();

    std::vector<QVector3D> ret;
    ret.reserve(n_segs);
    for ( int i = 0; i < n_segs; i++ )
    {
        float angle = 2 * M_PI / n_segs * i;
        ret.push_back(
            (base1 * std::cos(angle) + base2 * std::sin(angle)) * radius
        );
    }

    return ret;
}


/**
 * \brief Point in a sphere slice (ie: circle around the Z axis)
 * \param r Radius
 * \param a Angle
 * \param z Z coordinate
 */
inline QVector3D sphere_point(float r, float a, float z)
{
    return QVector3D(r * std::cos(a), r * std::sin(a), z);
}

/**
 * \brief Slice of a sphere centered at 0
 */
inline std::vector<QVector3D> sphere_slice(float radius, float z, int n_segs)
{
    std::vector<QVector3D> ret;
    ret.reserve(n_segs);
    float rcur = std::sqrt(radius * radius - z * z);

    for ( int seg = 0; seg < n_segs; seg++ )
    {
        float angle = M_PI * 2 / n_segs * seg;
        ret.push_back(sphere_point(rcur, angle, z));
    }

    return ret;
}

/**
 * \brief Z-coordinate of a ring in a sphere centered at 0
 * \param radius    Sphere radius
 * \param ring      Ring index
 * \param n_rings   Number of rings
 */
inline float sphere_z(float radius, int ring, int n_rings)
{
    return radius * std::cos(ring*M_PI/n_rings+M_PI);
}

inline std::vector<QVector3D> oriented_sphere_slice(float radius, float z, int n_segs, const QVector3D& direction)
{
    std::vector<QVector3D> ret = sphere_slice(radius, z, n_segs);
    QVector3D base1 = vector_normal(direction);
    QVector3D base2 = QVector3D::crossProduct(direction, base1).normalized();
    QVector3D base3 = direction.normalized();
    for ( QVector3D& v : ret )
    {
        v = base1 * v.x() + base2 * v.y() + base3 * v.z();
    }
    return ret;
}

} // namespace glax3d

#endif // GLAX3D_SHAPE_HELPERS_H
