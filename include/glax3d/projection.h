#ifndef GLAX3D_PROJECTION_H
#define GLAX3D_PROJECTION_H

#include <QObject>
#include <QMatrix4x4>
#include "glax3d/hecking_windoze.h"


namespace glax3d {

class Projection : public QObject
{
    Q_OBJECT

    /**
     * \property
     * \brief Horizontal angle for the frustum (perspective only)
     */
    Q_PROPERTY(double fov READ fov WRITE set_fov NOTIFY fov_changed)

    /**
     * \property
     * \brief Distance of the near plane of the frustum from the position of the camera
     */
    Q_PROPERTY(double near READ near WRITE set_near NOTIFY near_changed)

    /**
     * \property
     * \brief  Distance of the far plane of the frustum from the camera
     */
    Q_PROPERTY(double far READ far WRITE set_far NOTIFY far_changed)

    /**
     * \property
     * \brief Use orthogonal projection instead of perspective
     */
    Q_PROPERTY(bool ortho READ ortho WRITE set_ortho NOTIFY ortho_changed)

    /**
     * \property
     * \brief Scale of the orthographic projection (ortographic only)
     */
    Q_PROPERTY(double ortho_scale READ ortho_scale WRITE set_ortho_scale NOTIFY ortho_scale_changed)

    /**
     * \property
     * \brief Aspect ratio (width/height) of the viewport
     */
    Q_PROPERTY(double aspect READ aspect WRITE set_aspect NOTIFY aspect_changed)

public:
    Projection(double near = 0.1, double far = 200, double fov = 90,
               bool ortho = false, double ortho_scale = 0.1, double aspect = 1)
    : _fov(fov), _near(near), _far(far), _ortho(ortho), _ortho_scale(ortho_scale), _aspect(aspect)
    {
        rebuild();
    }

    void to_orthographic(double near = 0.1, double far = 200, double scale = 0.1)
    {
        _near = near;
        _far = far;
        _ortho_scale = scale;
        _ortho = true;
        emit ortho_changed(_ortho);
        emit far_changed(_far);
        emit near_changed(_near);
        emit ortho_scale_changed(_ortho_scale);
        rebuild();
    }

    void to_perspective(double near = 0.1, double far = 200, double fov = 0.1)
    {
        _near = near;
        _far = far;
        _fov = fov;
        _ortho = false;
        emit ortho_changed(_ortho);
        emit far_changed(_far);
        emit near_changed(_near);
        emit fov_changed(_fov);
        rebuild();
    }

    void resize(int w, int h)
    {
        set_aspect(qreal(w) / qreal(h ? h : 1));
    }

    double near() const
    {
        return _near;
    }

    double far() const
    {
        return _far;
    }

    double fov() const
    {
        return _fov;
    }

    double ortho_scale() const
    {
        return _ortho_scale;
    }

    bool ortho() const
    {
        return _ortho;
    }

    double aspect() const
    {
        return _aspect;
    }

public slots:
    void set_ortho(bool ortho)
    {
        _ortho = ortho;
        rebuild();
        emit ortho_changed(_ortho);
    }

    void set_ortho_scale(double ortho_scale)
    {
        _ortho_scale = ortho_scale;
        rebuild();
        emit ortho_scale_changed(_ortho_scale);
    }

    void set_fov(double fov)
    {
        _fov = fov;
        rebuild();
        emit fov_changed(_fov);
    }

    void set_far(double far)
    {
        _far = far;
        rebuild();
        emit far_changed(_far);
    }

    void set_near(double near)
    {
        _near = near;
        rebuild();
        emit near_changed(_near);
    }

    void set_aspect(double aspect)
    {
        _aspect = aspect;
        rebuild();
        emit aspect_changed(_aspect);
    }

    const QMatrix4x4& matrix() const
    {
        return _matrix;
    }

    void copy_from(const Projection& oth)
    {
        _matrix = oth._matrix;
        _fov = oth._fov;
        _near = oth._near;
        _far = oth._far;
        _ortho_scale = oth._ortho_scale;
        _aspect = oth._aspect;
        _ortho = oth._ortho;

        ortho_changed(_ortho);
        ortho_scale_changed(_ortho_scale);
        fov_changed(_fov);
        near_changed(_near);
        far_changed(_far);
        aspect_changed(_aspect);
    }

signals:
    void ortho_changed(bool);
    void ortho_scale_changed(double);
    void fov_changed(double);
    void near_changed(double);
    void far_changed(double);
    void aspect_changed(double);

private:
    void rebuild()
    {
        _matrix.setToIdentity();
        if ( _ortho )
        {
            _matrix.ortho(-1, 1, -1/_aspect, 1/_aspect, _near, _far);
            _matrix.scale(_ortho_scale);
        }
        else
        {
            _matrix.perspective(_fov, _aspect, _near, _far);
        }

    }

    QMatrix4x4 _matrix;
    double _fov = 90;
    double _near = 0.1;
    double _far = 200;
    bool _ortho = false;
    double _ortho_scale = 0.1;
    double _aspect = 1;
};

} // namespace glax3d


#endif // GLAX3D_PROJECTION_H
