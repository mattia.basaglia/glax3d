#ifndef GLAX3D_RENDERER_H
#define GLAX3D_RENDERER_H

#include <vector>

#include <QOpenGLFunctions>
#include <QMatrix4x4>

#include "glax3d/gl_types.h"

namespace glax3d {

class Renderer : public QOpenGLFunctions
{
public:
    /**
     * \brief Initialization of OpenGL functions
     */
    void initialize();

    /**
     * \pre initialize() has been called
     * \post The renderer is set up for rendering
     */
    void rendering_initialize();

    /**
     * \pre rendering_initialize() has been called
     * \returns the current transform matrix
     */
    QMatrix4x4& transform();

    /**
     * \pre rendering_initialize() has been called
     * \post a new transform matrix is pushed to the stack
     * \returns the current transform matrix
     */
    QMatrix4x4& push();


    /**
     * \pre push() has been called more times than pop()
     * \post the current matrix is removed from the stack
     * \returns the current transform matrix
     */
    QMatrix4x4& pop();

    /**
     * \brief Renders the given shape buffer
     * \pre rendering_initialize() has been called
     */
    template<class MaterialT>
    void render_buffer(MaterialT& material, const typename MaterialT::shape_buffer_type& buffer)
    {
        render_buffer(material, buffer, buffer.size());
    }

    template<class MaterialT>
    void render_buffer(MaterialT& material, const typename MaterialT::shape_buffer_type& buffer, GLsizei count)
    {
        buffer.bind_buffer();
        material.bind();
        material.prepare_buffer(buffer, transform());
        glDrawArrays(buffer.primitive(), 0, count);
    }

    template<class MaterialT, class IndexContainerT, class IndexT = typename IndexContainerT::value_type>
    void render_buffer(
        MaterialT& material,
        const typename MaterialT::shape_buffer_type& buffer,
        const IndexContainerT& indices,
        GLsizei count = -1
    )
    {
        buffer.bind_buffer();
        material.bind();
        material.prepare_buffer(buffer, transform());
        glDrawElements(buffer.primitive(), count == -1 ? indices.size() : count, gl_type_enum<IndexT>(), indices.data());
    }

    /**
     * \brief Quality hint for shapes (1 = best, 0 = worst)
     */
    float quality() const
    {
        return _quality;
    }

    void set_quality(float quality)
    {
        _quality = quality;
    }

private:
    std::vector<QMatrix4x4> _transforms;
    float _quality = 1;
};

} // namespace glax3d

#endif // GLAX3D_RENDERER_H
