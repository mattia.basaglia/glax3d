#ifndef GLAX3D_SHAPE_BUFFER_H
#define GLAX3D_SHAPE_BUFFER_H

#include <vector>

#include <QOpenGLBuffer>

#include "glax3d/widgets/widget.h"

namespace glax3d {


/**
 * \brief Base class for shape buffers
 *
 * Uses the curiously recurring template idiom to implement compile-time polymorphism
 *
 * \tparam Derived Derived class, must implement on_insert, on_clear, on_erase
 * \tparam PointType Structured point data
 */
template<class Derived, class PointType>
class ShapeBufferBase
{
public:
    using size_type = quint32;
    using value_type = PointType;
    using vector_type = std::vector<value_type>;
    using iterator = typename vector_type::const_iterator;

    ShapeBufferBase(int primitive, vector_type points = {})
        : _data(std::move(points)), _primitive(primitive)
    {
        derived_this()->on_insert(0, _data.size(), protected_tag{});
    }

    ShapeBufferBase(const ShapeBufferBase& oth)
        : _data(oth._data), _primitive(oth._primitive)
    {
        derived_this()->on_insert(0, _data.size(), protected_tag{});
    }

    ShapeBufferBase(const ShapeBufferBase&& oth)
        : _data(std::move(oth._data)), _primitive(oth._primitive)
    {
        derived_this()->on_insert(0, _data.size(), protected_tag{});
    }

    ShapeBufferBase& operator=(const ShapeBufferBase& oth)
    {
        assign_begin();
        _data = oth._data;
        _primitive = oth._primitive;
        assign_end();
        return *this;
    }

    ShapeBufferBase& operator=(ShapeBufferBase&& oth)
    {
        assign_begin();
        std::swap(_data, oth._data);
        _primitive = oth._primitive;
        assign_end();
        return *this;
    }

    /**
     * \brief OpenGL primitive
     */
    GLenum primitive() const
    {
        return _primitive;
    }

    /**
     * \brief Inserts a single point
     */
    void push_back(const value_type& pt)
    {
        _data.push_back(pt);
        clear_buffer();
        derived_this()->on_insert(_data.size()-1, _data.size(), protected_tag{});
    }

    /**
     * \brief Removes all points
     */
    void clear()
    {
        derived_this()->on_clear(protected_tag{});
        _data.clear();
        clear_buffer();
    }

    /**
     * \brief Creates/binds the OpenGL buffer
     */
    void bind_buffer() const
    {
        if ( !_buffer.isCreated() )
        {
            _buffer.create();
            _buffer.bind();
            _buffer.allocate(_data.data(), _data.size() * sizeof(value_type));
        }
        else
        {
            _buffer.bind();
        }
    }

    /**
     * \brief Replaces the stored points
     */
    void assign(vector_type points)
    {
        assign_begin();
        std::swap(_data, points);
        assign_end();
    }

    /**
     * \brief Adds new points from another buffer
     */
    void append(const ShapeBufferBase& oth)
    {
        append(oth._data);
    }

    /**
     * \brief Adds new points
     */
    void append(const vector_type& points)
    {
        append_begin(points.size());
        _data.insert(_data.end(), points.begin(), points.end());
        append_end(points.size());
    }

    /**
     * \brief Number of points
     */
    size_type size() const
    {
        return _data.size();
    }

    /**
     * \brief Reduces the number of points to the given size
     * \post this->size() <= \p size
     */
    void truncate(size_type size)
    {
        if ( size < _data.size() )
        {
            derived_this()->on_erase(size, _data.size(), protected_tag{});
            _data.erase(_data.begin()+size, _data.end());
        }
    }

    /**
     * \brief Updates te value of a point
     */
    void update(size_type index, const value_type& point)
    {
        update_begin(index, index+1);
        _data[index] = point;
        update_end(index, index+1);
    }

    /**
     * \brief Returns a reference to the point at \p index
     * \pre 0 <= index && index < size()
     */
    const value_type& operator[](size_type index) const
    {
        return _data[index];
    }

    /**
     * \brief Whethe there are 0 points
     */
    bool empty() const
    {
        return _data.empty();
    }

    /**
     * \brief Iterator to the first point
     */
    iterator begin() const
    {
        return _data.begin();
    }

    /**
     * \brief Iterator to the past the last point
     */
    iterator end() const
    {
        return _data.end();
    }

protected:
    /**
     * \brief For derived classes, to be called at the beginning of fuctions adding new points
     * \param extra_size number of points to be added
     */
    void append_begin(size_type extra_size)
    {
        _data.reserve(_data.size() + extra_size);
    }
    /**
     * \brief For derived classes, to be called when appending points one by one
     * \pre append_begin() called
     * \post The point has been added to the data, size() has increased by 1
     */
    template<class... Args>
    void append_point(Args&&... args)
    {
        _data.emplace_back(std::forward<Args>(args)...);
    }
    /**
     * \brief For derived classes, to be called at the beginning of fuctions adding new points
     * \param extra_size number of points to be added
     * \pre append_begin(extra_size) called once and append_point() called extra_size times
     * \post The appended points are ready to use
     */
    void append_end(size_type extra_size)
    {
        derived_this()->on_insert(_data.size()-extra_size, _data.size(), protected_tag{});
        clear_buffer();
    }

    /**
     * \brief For derived classes, to be called at the beginning of fuctions updating points
     * \param from index of the first point to update
     * \param to index past the last point to update
     * \pre 0 <= from && from < to && to < size()
     */
    void update_begin(size_type from, size_type to)
    {
        derived_this()->on_erase(from, to, protected_tag{});
    }

    /**
     * \brief For derived classes, to be called at the end of fuctions updating points
     * \param from index of the first point to update
     * \param to index past the last point to update
     * \pre update_begin(from, to) called
     */
    void update_end(size_type from, size_type to)
    {
        derived_this()->on_insert(from, to, protected_tag{});
        clear_buffer();
    }

    /**
     * \brief Returns a mutable reference to the point at \p index
     * \pre update_begin() called with \p index in its range
     */
    value_type& ref(size_type index)
    {
        return _data[index];
    }

    /**
     * \brief Struct to tag curiously recurring derived functions
     */
    struct protected_tag{};

private:
    /**
     * \brief Invalidates the OpenGL buffer
     */
    void clear_buffer()
    {
        if ( _buffer.isCreated() )
            _buffer.destroy();
    }

    /**
     * \brief To be called at the beginning of fuctions replacing all points
     */
    void assign_begin()
    {
        derived_this()->on_clear(protected_tag{});
    }

    /**
     * \brief To be called at the end of fuctions replacing all points
     */
    void assign_end()
    {
        derived_this()->on_insert(0, _data.size(), protected_tag{});
        clear_buffer();
    }

    Derived* derived_this()
    {
        return static_cast<Derived*>(this);
    }

    const Derived* derived_this() const
    {
        return static_cast<const Derived*>(this);
    }

    vector_type _data;
    mutable QOpenGLBuffer _buffer;
    int _primitive;
};


template<class PointType>
class ShapeBuffer : public ShapeBufferBase<ShapeBuffer<PointType>, PointType>
{
private:
    using parent_type = ShapeBufferBase<ShapeBuffer<PointType>, PointType>;
    using protected_tag = typename parent_type::protected_tag;

public:
    using size_type = typename parent_type::size_type;

public:
    using ShapeBufferBase<ShapeBuffer<PointType>, PointType>::ShapeBufferBase;

    void on_insert(size_type, size_type, protected_tag) noexcept {}
    void on_clear(protected_tag) noexcept {}
    void on_erase(size_type, size_type, protected_tag) noexcept {}

};


} // namespace glax3d

#endif // GLAX3D_SHAPE_BUFFER_H
