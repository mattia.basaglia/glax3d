#ifndef GLAX3D_EXCEPTION_H
#define GLAX3D_EXCEPTION_H

#include <exception>
#include <cstring>

#include <QString>

namespace glax3d {


class Glax3DError : public std::exception
{
public:
    explicit Glax3DError(QString message) noexcept
        : _message(std::move(message)), std_msg(_message.toStdString()) {}

    virtual const QString& message() const noexcept
    {
        return _message;
    }

    const char* what() const noexcept override
    {
        try{
            return std_msg.c_str();
        } catch (...) {
            return "";
        }
    }

private:
    QString _message;
    std::string std_msg;
};


class Glax3DPublicError : public Glax3DError
{
public:
    using Glax3DError::Glax3DError;
};

} // namespace glax3d
#endif // GLAX3D_EXCEPTION_H
