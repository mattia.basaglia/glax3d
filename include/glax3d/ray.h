#ifndef GLAX3D_RAY_H
#define GLAX3D_RAY_H

#include <QVector3D>

namespace glax3d {

class Ray
{
public:
    Ray(const QVector3D& start, const QVector3D& end)
        : _start(start), _end(end)
    {
        recalculate();
    }

    /**
     * \brief Starting point of the ray
     */
    const QVector3D& start() const
    {
        return _start;
    }

    void set_start(const QVector3D& v)
    {
        _start = v;
        recalculate();
    }

    /**
     * \brief End point of the ray
     */
    const QVector3D& end() const
    {
        return _end;
    }

    void set_end(const QVector3D& v)
    {
        _end = v;
        recalculate();
    }

    /**
     * \brief Vector with same length and direction as the ray
     */
    const QVector3D& vector_length() const
    {
        return _vector_length;
    }

    /**
     * \brief Unit direction vector
     */
    const QVector3D& direction() const
    {
        return _direction;
    }

    /**
     * \brief Point between start (ratio=0) and end (ratio=1)
     */
    QVector3D point_at(float ratio) const
    {
        return _vector_length * ratio + _start;
    }

    /**
     * \brief Point on the ray the given lengh away from the start
     */
    QVector3D point_at_length(float length) const
    {
        return _direction * length + _start;
    }

    float length() const
    {
        return _vector_length.length();
    }

private:
    void recalculate()
    {
        _vector_length = _end - _start;
        _direction = _vector_length.normalized();
    }

    QVector3D _start;
    QVector3D _end;
    QVector3D _vector_length;
    QVector3D _direction;
};

} // namespace glax3d

#endif // GLAX3D_RAY_H
