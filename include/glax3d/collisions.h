#ifndef GLAX3D_COLLISIONS_H
#define GLAX3D_COLLISIONS_H

#include <memory>
#include <type_traits>

#include <QMatrix4x4>

#include "glax3d/ray.h"

namespace glax3d {
namespace collisions {

class CollisionShape
{
public:
    virtual ~CollisionShape() = default;

    /**
     * \brief Checks for intersection between ray and the shape
     * \returns non-zero if the ray intersects the shape
     * \param[in]   ray     Ray to check for intersections
     * \param[out]  ratio   If not null and the function returns > 0, set to the
     *                      ratio for the intersection point closest to the
     *                      start of the ray.
     */
    virtual int intersects(const Ray& ray, float* ratio = nullptr) const = 0;

};


class EmptyCollision : public CollisionShape
{
public:
    int intersects(const Ray&, float* = nullptr) const override
    {
        return 0;
    }

    static const EmptyCollision& static_instance()
    {
        static EmptyCollision instance;
        return instance;
    }
};

class AxisAlignedBox : public CollisionShape
{
public:
    AxisAlignedBox(
        const QVector3D& point1,
        const QVector3D& point2
    ) : point1(point1), point2(point2)
    {}


    int intersects(const Ray& ray, float* ratio = nullptr) const override;

    bool intersects(const AxisAlignedBox& other) const;

    QVector3D point1;
    QVector3D point2;
};

class MultiCollision : public CollisionShape
{
public:
    template<class T, class... Args>
    int append(Args&&... args)
    {
        return append_priority<T>(0, std::forward<Args>(args)...);
    }

    template<class T, class... Args>
    int append_priority(int priority, Args&&... args)
    {
        shapes.push_back({priority, std::make_unique<T>(std::forward<Args>(args)...)});
        return shapes.size();
    }

    void clear()
    {
        shapes.clear();
    }

    int intersects(const Ray& ray, float* ratio = nullptr) const override;


    std::vector<std::pair<int, std::unique_ptr<CollisionShape>>> shapes;
};

class Sphere : public CollisionShape
{
public:
    Sphere(
        const QVector3D& center,
        float radius
    ) : center(center), radius(radius)
    {}


    int intersects(const Ray& ray, float* ratio = nullptr) const override;

    QVector3D center;
    float radius;
};


class Capsule : public CollisionShape
{
public:
    Capsule(
        const QVector3D& a,
        const QVector3D& b,
        float radius
    ) : a(a), b(b), radius(radius)
    {}


    int intersects(const Ray& ray, float* ratio = nullptr) const override;

    QVector3D a;
    QVector3D b;
    float radius;
};

} // namespace collisions
} // namespace glax3d

#endif // GLAX3D_COLLISIONS_H
