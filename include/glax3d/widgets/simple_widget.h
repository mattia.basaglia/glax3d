#ifndef GLAX3D_WIDGETS_WIDGETS_H
#define GLAX3D_WIDGETS_WIDGETS_H


#include "glax3d/widgets/widget.h"
#include "glax3d/shape_buffer.h"
#include "glax3d/collisions.h"
#include "glax3d/widgets/events.h"
#include "glax3d/widgets/widget_container.h"
#include "glax3d/renderer.h"

namespace glax3d::widgets {


template<class Derived, class Vector3DT>
class GenericSimpleWidget : public Widget
{
public:
    using Vector3D = Vector3DT;

    const Vector3D& origin() const
    {
        return _origin;
    }

    Intersection intersects(const Ray& ray) const override
    {
        Intersection intersect;
        intersect.intersection = collision_shape().intersects(ray, &intersect.ratio);
        if ( intersect.intersection )
            intersect.widget = const_cast<GenericSimpleWidget*>(this);
        return intersect;
    }

    virtual const collisions::CollisionShape& collision_shape() const
    {
        return collisions::EmptyCollision::static_instance();
    }

    void render(Renderer& params) const override
    {
        params.push().translate(QVector3D(_origin));
        on_render(params);
        params.pop();
    }

    void set_origin(const Vector3D& origin)
    {
        _origin = origin;
        on_drag_move();
        derived()->emit_origin_changed(origin);
    }

protected:
    using Generic = GenericSimpleWidget;

    enum class Axis {
        X, Y, Z, All, None
    };

    Axis drag_axis() const
    {
        return _axis;
    }

    void drag_start(const MouseEvent& ev, Axis axis)
    {
        _axis = axis < Axis::X || axis >= Axis::All ? Axis::All : axis;
        _ratio = ev.intersection.ratio;
        _grab = ev.intersect_point();
        _start_origin = _origin;
    }

    void drag_move(const MouseEvent& ev)
    {
        if ( _axis == Axis::None )
            return;

        Vector3D target = _start_origin + Vector3D(ev.ray.point_at(_ratio)) - _grab;
        if ( _axis == Axis::All )
            _origin = target;
        else
            _origin[int(_axis)] = target[int(_axis)];

        on_drag_move();
        derived()->emit_origin_dragged(_origin);
        derived()->emit_origin_changed(_origin);
    }

    void drag_stop()
    {
        _axis = Axis::None;
        derived()->emit_drag_completed(_start_origin, _origin);
    }

    virtual void on_drag_move() {};
    virtual void on_render(glax3d::Renderer& renderer) const = 0;

private:
    Derived* derived()
    {
        return static_cast<Derived*>(this);
    }

    Vector3D _origin;
    Axis _axis = Axis::None;
    float _ratio = 0;
    Vector3D _grab;
    Vector3D _start_origin;
};


/**
 * \brief Widget that can contain various shapes and has an offset
 */
class SimpleWidget : public GenericSimpleWidget<SimpleWidget, QVector3D>
{
    Q_OBJECT

    Q_PROPERTY(Vector3D origin READ origin WRITE set_origin NOTIFY origin_dragged)


public slots:
    void set_origin(const Vector3D& origin)
    {
        Generic::set_origin(origin);
    }

signals:
    void origin_dragged(const Vector3D& origin);
    void origin_changed(const Vector3D& origin);
    void drag_completed(const Vector3D& from, const Vector3D& to);

private:
    friend Generic;

    void emit_origin_dragged(const Vector3D& origin)
    {
        emit origin_dragged(origin);
    }

    void emit_origin_changed(const Vector3D& origin)
    {
        emit origin_changed(origin);
    }

    void emit_drag_completed(const Vector3D& from, const Vector3D& to)
    {
        emit drag_completed(from, to);
    }
};


} // namespace glax3d::widgets

#endif // GLAX3D_WIDGETS_WIDGETS_H
