#ifndef GLAX3D_WIDGETS_EVENTS_H
#define GLAX3D_WIDGETS_EVENTS_H

#include <QMouseEvent>

#include "glax3d/ray.h"

namespace glax3d::widgets {


class Widget;

/**
 * \brief data about widget intersection
 */
struct Intersection
{
    Widget* widget = nullptr;
    int intersection = 0;
    float ratio = -1;
    std::vector<Widget*> children;

    void push_parent(Widget* parent)
    {
        children.push_back(widget);
        widget = parent;
    }

    void pop_parent()
    {
        if ( children.empty() )
        {
            widget = nullptr;
        }
        else
        {
            widget = children.back();
            children.pop_back();
        }
    }

    operator bool() const
    {
        return widget;
    }
};

/**
 * \brief 3D mouse event data
 */
class MouseEvent
{
public:
    MouseEvent(
        const Ray& ray,
        QMouseEvent* me,
        const Intersection& intersection
    ) : ray(ray),
        intersection(intersection),
        type(me->type()),
        button(me->button()),
        buttons(me->buttons()),
        modifiers(me->modifiers())
    {}

    QVector3D intersect_point() const
    {
        return ray.point_at(intersection.ratio);
    }

    Ray ray;
    Intersection intersection;
    QEvent::Type type;
    Qt::MouseButton button;
    Qt::MouseButtons buttons;
    Qt::KeyboardModifiers modifiers;
};

} // namespace glax3d::widgets

#endif // GLAX3D_WIDGETS_EVENTS_H
