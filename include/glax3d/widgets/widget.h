#ifndef GLAX3D_WIDGETS_WIDGET_H
#define GLAX3D_WIDGETS_WIDGET_H

#include <memory>

#include <QObject>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

namespace glax3d{
class Ray;
class Renderer;

namespace widgets {
struct Intersection;
class MouseEvent;

/**
 * \brief 2D widget for an opengl view
 */
class Widget : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool focused READ focused WRITE set_focus NOTIFY focus_changed)

public:

    virtual ~Widget()
    {
        emit widget_destroyed(this);
    }

    virtual Intersection intersects(const Ray& ray) const = 0;

    virtual void mouse_event(const MouseEvent& ev);

    virtual void render(Renderer& params) const = 0;

    bool focused() const
    {
        return _focused;
    }

signals:
    void focus_changed(bool focused);
    void widget_destroyed(Widget* w);

public slots:
    void set_focus(bool focused);

protected:
    virtual void mouse_press_event(const MouseEvent&){}
    virtual void mouse_move_event(const MouseEvent&){}
    virtual void mouse_release_event(const MouseEvent&){}
    virtual void on_focus(bool){}

    bool _focused = false;
};


} // namespace widgets
} // namespace glax3d

#endif // GLAX3D_WIDGETS_WIDGET_H
