#ifndef GLAX3D_WIDGETS_WIDGET_CONTAINER_H
#define GLAX3D_WIDGETS_WIDGET_CONTAINER_H

#include <QVector>

#include "glax3d/widgets/widget.h"

class QMouseEvent;

namespace glax3d::widgets {

class WidgetContainerPrivate;

/**
 * \brief Widget containing multiple Widgets
 */
class WidgetContainer : public Widget
{
    Q_OBJECT

public:
    WidgetContainer();

    ~WidgetContainer();

    /**
     * \brief Adds a widget to be rendered
     */
    void append(Widget* child);
    /**
     * \brief Adds a widget and re-parents it
     */
    void add_child(Widget* child);
    /**
     * \brief Removes a widget to be rendered
     */
    void remove(Widget* child);

    Intersection intersects(const Ray& ray) const override;

    const QVector<Widget*>& children() const;

    void mouse_event(const MouseEvent& ev) override;

    void prepare_mouse_event(const Ray& ray, QMouseEvent* me);

    void render(Renderer& params) const override;

protected:
    void on_focus(bool focused) override;

private slots:
    void focus_test(bool focused);

private:
    std::unique_ptr<WidgetContainerPrivate> d;
};


} // namespace glax3d

#endif // GLAX3D_WIDGETS_WIDGET_CONTAINER_H
