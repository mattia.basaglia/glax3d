#ifndef GLAX3D_GL_TYPES
#define GLAX3D_GL_TYPES

#include <QOpenGLFunctions>

namespace glax3d {

template<class T>
constexpr GLenum gl_type_enum() noexcept;

template<> constexpr GLenum gl_type_enum<GLbyte>() noexcept { return GL_BYTE; }
template<> constexpr GLenum gl_type_enum<GLubyte>() noexcept { return GL_UNSIGNED_BYTE; }
template<> constexpr GLenum gl_type_enum<GLshort>() noexcept { return GL_SHORT; }
template<> constexpr GLenum gl_type_enum<GLushort>() noexcept { return GL_UNSIGNED_SHORT; }
template<> constexpr GLenum gl_type_enum<GLint>() noexcept { return GL_INT; }
template<> constexpr GLenum gl_type_enum<GLuint>() noexcept { return GL_UNSIGNED_INT; }
template<> constexpr GLenum gl_type_enum<GLfloat>() noexcept { return GL_FLOAT; }
template<> constexpr GLenum gl_type_enum<GLdouble>() noexcept { return GL_DOUBLE; }

} // namespace glax3d

#endif // GLAX3D_GL_TYPES
