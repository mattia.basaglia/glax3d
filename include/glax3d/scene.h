#ifndef GLAX3D_SCENE_H
#define GLAX3D_SCENE_H

#include "glax3d/widgets/widget_container.h"
#include "glax3d/camera.h"
#include "glax3d/projection.h"
#include "glax3d/renderer.h"

namespace glax3d {

class Scene
{
public:
    template<class T, class... Args>
    void add_widget(Args&&... args)
    {
        _widget.add_child(new T(std::forward<Args>(args)...));
    }

    Projection& projection()
    {
        return _projection;
    }

    const Projection& projection() const
    {
        return _projection;
    }

    QRect viewport()
    {
        return _viewport;
    }

    void set_viewport_pos(int x, int y)
    {
        _viewport.moveTo(x, y);
    }

    void resize(int w, int h)
    {
        _viewport.setWidth(w);
        _viewport.setHeight(h);
        _projection.resize(w, h);
    }

    Camera& camera()
    {
        return _camera;
    }

    widgets::WidgetContainer& widget()
    {
        return _widget;
    }

    QMatrix4x4 view_matrix() const
    {
        return _projection.matrix() * _camera.matrix();
    }

    void initialize()
    {
        _renderer.initialize();
    }

    void render() const
    {
        _renderer.glViewport(_viewport.x(), _viewport.y(), _viewport.width(), _viewport.height());
        _renderer.rendering_initialize();
        _widget.render(_renderer);
    }


    void drag_init_orbit(const QPointF& mouse_pos)
    {
        _mouse_start_2d = QVector2D(mouse_pos);
        _mouse_start_3d = _camera.angles();
    }

    void drag_init_pan(const QPointF& mouse_pos)
    {
        _mouse_start_2d = QVector2D(mouse_pos);
        _mouse_start_3d = _camera.target();
    }

    void drag_orbit(const QPointF& mouse_pos)
    {
        QVector2D norm_delta = (QVector2D(mouse_pos) - _mouse_start_2d) /
                               QVector2D(_viewport.width(), _viewport.height());
        _camera.set_angles(QVector3D(
            _mouse_start_3d.x() + norm_delta.y() * 360,
            _mouse_start_3d.y() + norm_delta.x() * 360,
            0
        ));
    }

    void drag_pan_xy(const QPointF& mouse_pos)
    {
        QVector2D delta = QVector2D(mouse_pos) - _mouse_start_2d;
        QVector3D proj = project(_mouse_start_3d);
        proj -= delta;
        QVector3D dest = unproject(proj);
        _camera.set_target(dest);
    }

    void drag_pan_xz(const QPointF& mouse_pos)
    {
        QVector2D delta = QVector2D(mouse_pos) - _mouse_start_2d;
        QVector3D proj = project(_mouse_start_3d);
        proj.setX(proj.x() - delta.x());
        QVector3D dest = unproject(proj);
        dest += _camera.forward() * (delta.y() / _viewport.height() * _camera.distance() * 2);
        _camera.set_target(dest);
    }

    void mouse_event(QMouseEvent *event)
    {
        _widget.prepare_mouse_event(unproject_ray(event->pos()), event);
    }

    /**
     * \brief 2D to 3D projection
     * \param p Poin in widget coordinates
     * \param depth Value in [0 (near), 1 (far)]
     */
    QVector3D unproject(const QPoint& p, float depth) const
    {
        return unproject(QVector3D(p.x(), p.y(), depth));
    }

    QVector3D unproject(const QVector3D& p) const
    {
        return QVector3D(p.x(), _viewport.height() - p.y(), p.z())
            .unproject(_camera.matrix(), _projection.matrix(), _viewport);
    }

    /**
     * \brief Projects a 2D point into a 3D ray
     */
    glax3d::Ray unproject_ray(const QPoint& p) const
    {
        return {unproject(p, 0), unproject(p, 1)};
    }

    QVector3D project(const QVector3D& p)
    {
        QVector3D v = QVector3D(p).project(_camera.matrix(), _projection.matrix(), _viewport);
        v.setY(_viewport.height() - v.y());
        return v;
    }

    Renderer& renderer()
    {
        return _renderer;
    }

    void copy_view(const Scene& oth)
    {
        _camera.copy_from(oth._camera);
        _projection.copy_from(oth._projection);
    }

private:
    mutable Renderer _renderer;
    widgets::WidgetContainer _widget;
    Camera _camera;
    Projection _projection;
    QRect _viewport;

    QVector3D _mouse_start_3d;
    QVector2D _mouse_start_2d;

};

} // namespace glax3d

#endif // GLAX3D_SCENE_H
