#ifndef GLAX3D_CAMERA_H
#define GLAX3D_CAMERA_H

#include <QObject>
#include <QMatrix4x4>

namespace glax3d {

class Camera : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QVector3D target READ target WRITE set_target)
    Q_PROPERTY(QVector3D angles READ angles WRITE set_angles NOTIFY angles_changed)

public:
    explicit Camera(float distance=10, const QVector3D& up = {0, 1, 0})
        : _distance(distance)
    {
        set_global_up(up);
    }

    void set_global_up(const QVector3D& up)
    {
        QVector3D stdup(0, 1, 0);
        QVector3D v2 = up.normalized();
        if ( v2 != stdup )
        {
            QVector3D v1 = stdup;
            QVector3D v3 = QVector3D::crossProduct(v1, v2).normalized();
            QVector3D v4 = QVector3D::crossProduct(v3, v1);
            float m1a[16] = {
                v1[0], v1[1], v1[2], 0,
                v4[0], v4[1], v4[2], 0,
                v3[0], v3[1], v3[2], 0,
                0    , 0    , 0    , 1
            };
            QMatrix4x4 m1(m1a);
            float c = QVector3D::dotProduct(v2, v1);
            float s = QVector3D::dotProduct(v2, v4);
            float m2a[16] = {
                 c, s, 0, 0,
                -s, c, 0, 0,
                 0, 0, 1, 0,
                 0, 0, 0, 1
            };
            QMatrix4x4 m2(m2a);

            _uprot = m1.inverted() * m2 * m1;
        }
        else
        {
            _uprot.setToIdentity();
        }

        build_matrix();
    }

    QVector3D forward()
    {
        QMatrix4x4 m = _matrix.inverted();
        return (m * QVector3D() - m * QVector3D(0, 0, 1)).normalized();
    }


    const QMatrix4x4& matrix() const
    {
        return _matrix;
    }

    const QVector3D& target() const
    {
        return _target;
    }

    qreal distance() const
    {
        return _distance;
    }

    const QVector3D& angles() const
    {
        return _angles;
    }


    void copy_from(const Camera& oth)
    {
        _matrix = oth._matrix;
        _target = oth._target;
        _angles = oth._angles;
        _distance = oth._distance;
        _uprot = oth._uprot;
        emit angles_changed(_angles);
    }

public slots:
    void set_target(const QVector3D& target)
    {
        _target = target;
        build_matrix();
    }

    void set_distance(qreal distance)
    {
        _distance = distance;
        build_matrix();
    }

    void set_angles(const QVector3D& angles)
    {
        _angles = angles;
        emit angles_changed(angles);
        build_matrix();
    }

signals:
    void angles_changed(const QVector3D& angles);

private:
    void build_matrix()
    {
        _matrix.setToIdentity();
        _matrix.translate(0, 0, -_distance);
        _matrix.rotate(_angles.x(), 1, 0, 0);
        _matrix.rotate(_angles.z(), 0, 0, 1);
        _matrix.rotate(_angles.y(), 0, 1, 0);
//         _matrix.rotate(-90, 1, 0, 0);
        _matrix *= _uprot;
        _matrix.translate(-_target);
    }

    QMatrix4x4 _matrix;
    QVector3D _target;
    QVector3D _angles;
    qreal _distance;
    QMatrix4x4 _uprot;
};


} // namespace glax3d

#endif // GLAX3D_CAMERA_H
