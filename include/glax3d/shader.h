#ifndef GLAX3D_SHADER_H
#define GLAX3D_SHADER_H

#include <unordered_map>

#include <QOpenGLShaderProgram>
#include <QOpenGLExtraFunctions>

#include "glax3d/exception.h"
#include "glax3d/hecking_windoze.h"

namespace glax3d {

class Shader;
class Attribute;
class Uniform;

class BufferValue
{
public:
    constexpr BufferValue(GLenum type, int offset, int tuple_size, int stride = 0) noexcept
        : type(type), offset(offset), tuple_size(tuple_size), stride(stride)
    {}

private:
    friend Attribute;
    GLenum type;
    int offset;
    int tuple_size;
    int stride;
};

template<class... Args>
class ArrayValue
{
public:
    ArrayValue(Args... args)
        : args{args...}
    {}

private:
    friend Attribute;
    friend Uniform;
    std::tuple<Args...> args;
};


template<class... Args>
class ScalarValue
{
public:
    ScalarValue(Args... args)
        : args{args...}
    {}

private:
    friend Attribute;
    friend Uniform;
    std::tuple<Args...> args;
};


class Attribute
{
public:

    /**
     * \brief Creates an invalid attribute
     */
    constexpr Attribute() noexcept = default;
    constexpr Attribute& operator=(const Attribute&) noexcept = default;
    constexpr Attribute& operator=(Attribute&&) noexcept = default;

    template<class Arg>
    void operator<<(Arg&& arg)
    {
        set(arg);
    }

    int location() const noexcept
    {
        return _location;
    }

    template<class Arg>
    auto set(Arg&& arg) -> decltype(std::declval<QOpenGLShaderProgram*>()->setAttributeValue(0, std::forward<Arg>(arg)))
    {
        _program->setAttributeValue(_location, std::forward<Arg>(arg));
    }

    template<class... Args>
    void set(const ScalarValue<Args...>& args)
    {
        set_scalar(args.args, std::index_sequence_for<Args...>{});
    }

    template<class... Args>
    void set(const ArrayValue<Args...>& args)
    {
        set_array(args.args, std::index_sequence_for<Args...>{});
    }

    void set(const BufferValue& buf)
    {
        _program->setAttributeBuffer(_location, buf.type, buf.offset, buf.tuple_size, buf.stride);
    }

    void enable_array()
    {
        _program->enableAttributeArray(_location);
    }

private:
    template<class... Args, std::size_t... Is>
    void set_array(const std::tuple<Args...>& args, std::index_sequence<Is...>)
    {
        _program->setAttributeArray(_location, std::get<Is>(args)...);
    }
    template<class... Args, std::size_t... Is>
    void set_scalar(const std::tuple<Args...>& args, std::index_sequence<Is...>)
    {
        _program->setAttributeValue(_location, std::get<Is>(args)...);
    }

    constexpr Attribute(QOpenGLShaderProgram* program, int location) noexcept
    : _program(program), _location(location)
    {}
    friend Shader;
    QOpenGLShaderProgram* _program = nullptr;
    int _location = -1;
};

class Uniform
{
public:
    /**
     * \brief Creates an invalid uniform
     */
    constexpr Uniform() noexcept = default;

    constexpr Uniform& operator=(const Uniform&) noexcept = default;
    constexpr Uniform& operator=(Uniform&&) noexcept = default;

    template<class Arg>
    void operator<<(Arg&& arg)
    {
        set(std::forward<Arg>(arg));
    }

    int location() const noexcept
    {
        return _location;
    }

    template<class Arg>
    auto set(Arg&& arg) -> decltype(std::declval<QOpenGLShaderProgram*>()->setUniformValue(0, std::forward<Arg>(arg)))
    {
        _program->setUniformValue(_location, std::forward<Arg>(arg));
    }

    template<class... Args>
    void set(const ScalarValue<Args...>& args)
    {
        set_scalar(args.args, std::index_sequence_for<Args...>{});
    }

    template<class... Args>
    void set(const ArrayValue<Args...>& args)
    {
        set_array(args.args, std::index_sequence_for<Args...>{});
    }

private:
    template<class... Args, std::size_t... Is>
    void set_array(const std::tuple<Args...>& args, std::index_sequence<Is...>)
    {
        _program->setUniformValueArray(_location, std::get<Is>(args)...);
    }

    template<class... Args, std::size_t... Is>
    void set_scalar(const std::tuple<Args...>& args, std::index_sequence<Is...>)
    {
        _program->setUniformValue(_location, std::get<Is>(args)...);
    }

    constexpr Uniform(QOpenGLShaderProgram* program, int location) noexcept
    : _program(program), _location(location)
    {}
    friend Shader;
    QOpenGLShaderProgram* _program = nullptr;
    int _location = -1;
};

#ifdef GL_UNIFORM
#define GLAX3D_GL_NEW_STYLE 1
#define GLAX3D_UNIFORM GL_UNIFORM
#define GLAX3D_ATTRIBUTE GL_PROGRAM_INPUT
#else
#define GLAX3D_GL_NEW_STYLE 0
#define GLAX3D_UNIFORM GL_ACTIVE_UNIFORMS
#define GLAX3D_ATTRIBUTE GL_ACTIVE_ATTRIBUTES
#endif

class Shader
{
public:
    /**
     * \brief Whether the shader has been loaded correctly
     */
    bool loaded() const
    {
        return _loaded;
    }

    /**
     * \brief Loads the shaders from filenames
     * \pre !loaded()
     * \post loaded()
     */
    void compile(const QString& vertex, const QString& fragment)
    {

        if ( !_program.addShaderFromSourceFile(QOpenGLShader::Vertex, vertex) )
            throw Glax3DPublicError(QObject::tr("Could not load vertex shader\n%1").arg(vertex));

        if ( !_program.addShaderFromSourceFile(QOpenGLShader::Fragment, fragment) )
            throw Glax3DPublicError(QObject::tr("Could not load fragment shader\n%1").arg(fragment));

        if ( !_program.link() )
            throw Glax3DPublicError(QObject::tr("Could not compile shaders"));

        _loaded = true;
    }

    /**
     * \brief load_shaders() has been called
     * \returns the compiled shader program
     */
    QOpenGLShaderProgram& program()
    {
        return _program;
    }

    void bind()
    {
        _program.bind();
    }

    void unbind()
    {
        _program.release();
    }

    Attribute attribute(const char* name)
    {
        return Attribute(&_program, attribute_location(name));
    }

    int attribute_location(const char* name)
    {
        return _program.attributeLocation(name);
    }

    Uniform uniform(const char* name)
    {
        return Uniform(&_program, uniform_location(name));
    }

    int uniform_location(const char* name)
    {
        return _program.uniformLocation(name);
    }

    std::unordered_map<std::string, Uniform> get_uniform_map()
    {
        return get_map<Uniform>(GLAX3D_UNIFORM);
    }

    std::unordered_map<std::string, Attribute> get_attribute_map()
    {
        return get_map<Attribute>(GLAX3D_ATTRIBUTE);
    }

    bool is_linked() const
    {
        return _program.isLinked();
    }

    void ensure_linked()
    {
        if ( !_program.isLinked() )
            _program.link();
    }

private:
    template<class ParamT>
    std::unordered_map<std::string, ParamT> get_map(GLenum interface)
    {
        std::unordered_map<std::string, ParamT> ret;

        int prog_id = _program.programId();
        GLint count = 0;
#if GLAX3D_GL_NEW_STYLE == 1
        funcs().glGetProgramInterfaceiv(prog_id, interface, GL_ACTIVE_RESOURCES, &count);

        std::array<GLchar, 256> name_data;
        constexpr int n_props = 1;
        std::array<GLenum, n_props> properties = {
            GL_NAME_LENGTH
            // could fetch more data
        };
        std::array<GLint, n_props> values;

        for(int param_index = 0; param_index < count; ++param_index)
        {
            funcs().glGetProgramResourceiv(
                prog_id, interface, param_index, n_props,
                &properties[0], values.size(), nullptr, &values[0]
            );

            auto name_length = values[0];
            funcs().glGetProgramResourceName(
                prog_id, interface, param_index, name_data.size(), nullptr, &name_data[0]
            );
            std::string key(name_data.begin(), name_data.end() + name_length - 1);
            ret[key] = ParamT(&_program, param_index);
        }
#else
        glGetProgramiv(prog_id, interface, &count);
        std::array<GLchar, 256> name_data;
        auto get_func = interface == GLAX3D_UNIFORM ? glGetActiveUniform : glGetActiveAttrib;
        for ( int param_index = 0; param_index < count; ++param_index )
        {
            GLint array_size = 0;
            GLenum type = 0;
            GLsizei name_length = 0;
            get_func(prog_id, param_index, name_data.size(), &name_length, &array_size, &type, &name_data[0]);
            std::string key(name_data.begin(), name_data.end() + name_length - 1);
            ret[key] = ParamT(&_program, param_index);
        }
#endif

        return ret;
    }

    QOpenGLExtraFunctions& funcs()
    {
        return *QOpenGLContext::currentContext()->extraFunctions();
    }

    bool _loaded = false;
    QOpenGLShaderProgram _program;
};

} // namespace glax3d

#endif // GLAX3D_SHADER_H
