#ifndef GLAX3D_OCTREE_H
#define GLAX3D_OCTREE_H

#include <array>
#include <unordered_map>

#include "glax3d/shape_buffer.h"
#include "glax3d/collisions.h"
#include "glax3d/gl_types.h"

class test_octree;

namespace glax3d {

/**
 * \brief Traits for a vertex in a octree
 */
template<class VertexType, class ShapeBufferT=ShapeBuffer<VertexType>>
class VertexTraits
{
public:
    using vertex_type = VertexType;
    using scalar_type = typename vertex_type::scalar_type;
    using shape_buffer_type = ShapeBufferT;
    using size_type = typename shape_buffer_type::size_type;
    using index_type = GLuint;
    static constexpr int gl_type = gl_type_enum<index_type>();
    struct coord_type {
        scalar_type x;
        scalar_type y;
        scalar_type z;
    };

    static scalar_type x(const vertex_type& v) noexcept(noexcept(v.x()))
    {
        return v.x();
    }

    static scalar_type y(const vertex_type& v) noexcept(noexcept(v.y()))
    {
        return v.y();
    }

    static scalar_type z(const vertex_type& v) noexcept(noexcept(v.z()))
    {
        return v.z();
    }

    static coord_type coord(const vertex_type& v) noexcept(
        noexcept(VertexTraits::x(v)) &&
        noexcept(VertexTraits::y(v)) &&
        noexcept(VertexTraits::z(v))
    )
    {
        return {x(v), y(v), z(v)};
    }

    static QVector3D coord_vec(const coord_type& c)
    {
        return QVector3D(c.x, c.y, c.z);
    }

    static QVector3D vec3(const vertex_type& v)
    {
        return coord_vec(coord(v));
    }
};

/**
 * \brief Helper class to store Octree node data.
 *        Specialize for types with specific requirements.
 */
template<class NodeData>
class NodeDataStorage
{
public:
    /**
     * \brief Reference to the stored data
     */
    NodeData& get() noexcept
    {
        return value;
    }

    /**
     * \brief Get the value of the stored data
     */
    const NodeData& get() const noexcept
    {
        return value;
    }

    /**
     * \brief Update the stored data
     */
    template<class T>
    void set(T&& v)
    {
        value = std::forward<T>(v);
    }

private:
    NodeData value = NodeData();
};

/**
 * \brief NodeDataStorage specialization for when no data is needed
 */
template<>
class NodeDataStorage<void>
{
public:
    void get() const noexcept {}
    void set() {}
};


template<
    class VertexType,
    class VertexTraitsType,
    class NodeData
>
class Octree;

/**
 * \brief Octree node
 */
template<
    class VertexType,
    class VertexTraitsType=VertexTraits<VertexType>,
    class NodeData=void
>
class OctreeNode
{
public:
    using traits_type = VertexTraitsType;
    using vertex_type = typename traits_type::vertex_type;
    using scalar_type = typename traits_type::scalar_type;
    using coord_type = typename traits_type::coord_type;
    using shape_buffer_type = typename traits_type::shape_buffer_type;
    using index_type = typename traits_type::index_type;
    using size_type = typename traits_type::size_type;
    using index_vector_type = std::vector<index_type>;

    class iterator
    {
    public:
        using value_type = OctreeNode;
        using referece = const value_type&;
        using pointer = const value_type*;

        iterator(const OctreeNode* node, bool leaves_only)
            : leaves_only(leaves_only)
        {
            stack.push_back(node);
            if ( leaves_only )
                find_leaf();
        }

        iterator(){}

        iterator& operator++()
        {
            step();
            if ( leaves_only )
                find_leaf();
            return *this;
        }

        iterator operator++(int)
        {
            auto copy = *this;
            ++*this;
            return copy;
        }

        bool operator==(const iterator& it) const
        {
            if ( end() || it.end() )
                return end() && it.end();
            return node() == it.node();
        }

        bool operator!=(const iterator& it) const
        {
            return !(*this == it);
        }

        bool end() const
        {
            return stack.empty();
        }

        referece operator*() const
        {
            return *node();
        }

        pointer operator->() const
        {
            return node();
        }

    private:
        void find_leaf()
        {
            while ( !end() && !node()->leaf() )
                step();
        }

        void step()
        {
            if ( node()->leaf() || next_child == 8 )
                return step_up();

            stack.push_back(next());
            next_child = 0;
        }

        void step_up()
        {
            auto old = node();
            stack.pop_back();

            if ( stack.empty() )
                return;

            // note: node() can't be a leaf because it's the parent of old

            for ( next_child = 0; next_child < 8; next_child++ )
            {
                if ( next() == old )
                {
                    next_child++;
                    break;
                }
            }
            if ( next_child == 8 )
                return step_up();
            step();
        }

        const OctreeNode* node() const
        {
            return stack.back();
        }

        const OctreeNode* next() const
        {
            return &node()->_children[next_child];
        }

        std::vector<const OctreeNode*> stack;
        int next_child = 0;
        bool leaves_only = true;
        friend test_octree;
    };

    OctreeNode(coord_type min, coord_type max)
        : _min(min), _max(max), _mid{
            (_max.x + _min.x) / 2,
            (_max.y + _min.y) / 2,
            (_max.z + _min.z) / 2
        }
    {}
    // move-only to ensure we don't make expensive copies
    OctreeNode(const OctreeNode&) = delete;
    OctreeNode& operator=(const OctreeNode&) = delete;
    OctreeNode(OctreeNode&&) = default;
    OctreeNode& operator=(OctreeNode&&) = default;

    /**
     * \brief whether the node is a leaf
     */
    bool leaf() const noexcept
    {
        return _children.empty();
    }

    /**
     * \brief Minimum coordinates for the octree node
     */
    QVector3D min() const
    {
        return traits_type::coord_vec(_min);
    }

    /**
     * \brief Maximum coordinates for the octree node
     */
    QVector3D max() const
    {
        return traits_type::coord_vec(_max);
    }

    /**
     * \brief Central point of the node's bounding box
     */
    QVector3D center() const
    {
        return traits_type::coord_vec(_mid);
    }

    /**
     * \brief Bounding box of the node
     */
    collisions::AxisAlignedBox box() const
    {
        return {min(), max()};
    }

    /**
     * \brief Number of vertices in the node or its children
     */
    size_type size() const noexcept
    {
        return _size;
    }

    /**
     * \brief Recursively invokes the callback (depth first)
     */
    template<class Callback>
    void update_data(const Callback& callback)
    {
        for ( auto& child : _children )
            child.update_data(callback);
        callback(*this);
    }

    /**
     * \brief List of child nodes
     */
    const std::vector<OctreeNode>& children() const
    {
        return _children;
    }

    /**
     * \brief Update node data
     */
    template<class T>
    void set_data(T&& v)
    {
        _data.set(std::forward<T>(v));
    }

    /**
     * \brief Reference to the node data
     */
    decltype(auto) data()
    {
        return _data.get();
    }

    /**
     * \brief Get the node data
     */
    decltype(auto) get_data() const
    {
        return _data.get();
    }

    /**
     * \brief Returns whether \p ct is within the node
     */
    bool in_bounds(const coord_type& ct) const noexcept
    {
        return ct.x <  _max.x &&
               ct.x >= _min.x &&
               ct.y <  _max.y &&
               ct.y >= _min.y &&
               ct.z <  _max.z &&
               ct.z >= _min.z;
    }

    /**
     * \pre leaf()
     * \returns the indices stored in the node
     */
    const index_vector_type& indices() const
    {
        return _indices;
    }

private:
    /**
     * \brief Remove all indices
     * \note To be called by Octree
     * \post empty(),
     *       If the conditions are met, this node might get merged back into a leaf
     */
    void clear(
        size_type merge_count,
        scalar_type max_size
    )
    {
        if ( leaf() )
        {
            _indices.clear();
        }
        else
        {
            if (
                _max.x - _min.x <= max_size ||
                _max.y - _min.y <= max_size ||
                _max.z - _min.z <= max_size
            )
            {
                _children.clear();
            }
            else
            {
                for ( auto& c : _children )
                    c.clear(merge_count, max_size);
            }
        }
        _size = 0;
    }

    /**
     * \brief Remove some of the indices of the given buffer
     * \note To be called by Octree
     * \post The given indices are longer stored on this node or any of its descendants,
     *       If the conditions are met, this node might get merged back into a leaf
     */
    size_type remove_indices(
        index_type from_index,
        index_type to_index,
        size_type merge_count,
        scalar_type max_size
    )
    {
        if ( leaf() )
        {
            auto rmit = std::remove_if(
                _indices.begin(),
                _indices.end(),
                [from_index, to_index](index_type v){
                    return v >= from_index && v < to_index;
                }
            );
            size_type diff = _indices.end() - rmit;
            _indices.erase(rmit, _indices.end());
            _size -= diff;
            return diff;
        }
        else
        {
            size_type diff = 0;
            for ( auto& child : _children )
                diff += child.remove_indices(from_index, to_index, merge_count, max_size);
            _size -= diff;
            // children shoul already have been merged by the recursive remove_indices()
            if ( _size < merge_count && (
                _max.x - _min.x <= max_size ||
                _max.y - _min.y <= max_size ||
                _max.z - _min.z <= max_size
            ) )
            {
                merge();
            }
            return diff;
        }
    }

    /**
     * \brief Inserts and index into the node
     * \note To be called by Octree
     * \pre in_bounds(p)
     * \post The index is in the node (or one of its children)
     *       If the conditions are met, the node might be split
     */
    template<class ContainerT>
    void insert(
        const ContainerT& buffer,
        const coord_type& p,
        index_type index,
        size_type split_count,
        scalar_type min_size
    )
    {
        _size++;
        if ( leaf() )
        {
            _indices.push_back(index);
            if ( _size > split_count && (
                _max.x - _min.x > min_size ||
                _max.y - _min.y > min_size ||
                _max.z - _min.z > min_size
            ))
            {
                split(buffer, split_count, min_size);
            }
        }
        else
        {
            _children[child_index(p)].insert(buffer, p, index, split_count, min_size);
        }
    }

    /**
     * \brief Grows the node to include the point
     * \pre !in_bounds(p)
     * \post *this has transfered all its data to the correct child of the returned node
     * \returns A node to replace *this, with twice its size
     * \note To be called by Octree
     */
    OctreeNode expand(const coord_type& p)
    {
        int self_index;
        coord_type new_mid;
        std::tie(new_mid, self_index) = expand_direction(p);
        coord_type half_size {
            _max.x - _min.x,
            _max.y - _min.y,
            _max.z - _min.z,
        };
        OctreeNode parent(
            coord_type{
                new_mid.x - half_size.x,
                new_mid.y - half_size.y,
                new_mid.z - half_size.z
            },
            coord_type{
                new_mid.x + half_size.x,
                new_mid.y + half_size.y,
                new_mid.z + half_size.z
            }
        );

        parent._size = _size;

        parent.split_create_children();
        parent._children[self_index] = std::move(*this);
        return parent;
    }

    /**
     * \pre All children are leaves
     * \post \b this node is a leaf
     */
    void merge()
    {
        for ( auto& child : _children )
        {
            _indices.insert(_indices.end(), child._indices.begin(), child._indices.end());
        }
        _children.clear();
    }

    /**
     * \brief Finds the data to where to expand to reach \p p
     * \returns A pair, with the coordinates of the new midpoint for the parent node
     *          and the index of \b this among the children of the parent node
     */
    std::pair<coord_type, int> expand_direction(const coord_type& p) const
    {
        int self_index = 0;
        coord_type new_mid = _max;
        // self_index: could be any (low/high refer to the box compared to p)

        if ( _min.y > p.y )
        {
            self_index += 4;
            new_mid.y = _min.y;
        }

        // self_index: low y {0123} high y {4567}

        if ( _min.x > p.x )
        {
            self_index += 2;
            new_mid.x = _min.x;
        }

        // index: low x, low y {01} high x, low y {23} low x, high y {45} high x high y {67}

        if ( _min.z > p.z )
        {
            self_index += 1;
            new_mid.z = _min.z;
        }

        return {new_mid, self_index};
    }

    /**
     * \brief Index of the node \p p among the children of this node
     * \pre in_bounds(p) && !leaf()
     */
    int child_index(const coord_type& p) const
    {
        int index = 0;

        // index: could be any

        // {0123} are low y, {4567} high y
        if ( p.y >= _mid.y )
            index += 4;

        // index: low y {0123} high y {4567}

        // {01}&{45} are low x, {23}&{67} are high x
        if ( p.x >= _mid.x )
            index += 2;

        // index: low x, low y {01} high x, low y {23} low x, high y {45} high x high y {67}

        // {0246} are low z, {1357} are high z
        if ( p.z >= _mid.z )
            index += 1;

        return index;
    }

    /**
     * \brief Creates the child nodes
     */
    void split_create_children()
    {
        _children.reserve(8);
        // 0: x < mid, y < mid, z < mid
        _children.emplace_back(_min, _mid);
        // 1: x < mid, y < mid, z >= mid
        _children.emplace_back(coord_type{_min.x, _min.y, _mid.z}, coord_type{_mid.x, _mid.y, _max.z});
        // 2: x >= mid, y < mid, z < mid
        _children.emplace_back(coord_type{_mid.x, _min.y, _min.z}, coord_type{_max.x, _mid.y, _mid.z});
        // 3: x >= mid, y < mid, z >= mid
        _children.emplace_back(coord_type{_mid.x, _min.y, _mid.z}, coord_type{_max.x, _mid.y, _max.z});

        // 4: x < mid, y >= mid, z < mid
        _children.emplace_back(coord_type{_min.x, _mid.y, _min.z}, coord_type{_mid.x, _max.y, _mid.z});
        // 5: x < mid, y >= mid, z >= mid
        _children.emplace_back(coord_type{_min.x, _mid.y, _mid.z}, coord_type{_mid.x, _max.y, _max.z});
        // 6: x >= mid, y >= mid, z < mid
        _children.emplace_back(coord_type{_mid.x, _mid.y, _min.z}, coord_type{_max.x, _max.y, _mid.z});
        // 7: x >= mid, y >= mid, z >= mid
        _children.emplace_back(_mid, _max);
    }

    /**
     * \brief Splits the node into children and reassigns the stored data
     * \pre leaf()
     * \post !leaf(), all the indices are moved the correct children
     */
    template <class ContainerT>
    void split(const ContainerT& buffer, size_type split_count, scalar_type min_size)
    {
        split_create_children();

        for ( auto index : _indices )
        {
            auto p = traits_type::coord(buffer[index]);
            _children[child_index(p)].insert(buffer, p, index, split_count, min_size);
        }

        _indices.clear();
    }

    OctreeNode deep_copy() const
    {
        OctreeNode copy(_min, _max);
        copy._size = _size;
        if ( leaf() )
        {
            copy._indices = _indices;
        }
        else
        {
            copy._children.reserve(_children.size());
            for ( const auto& c : _children )
                copy._children.push_back(c.deep_copy());
        }

        return copy;
    }

    coord_type _min;
    coord_type _max;
    coord_type _mid;
    std::vector<OctreeNode> _children;
    index_vector_type _indices;
    size_type _size = 0;
    NodeDataStorage<NodeData> _data;


    friend test_octree;
    friend Octree<VertexType, VertexTraitsType, NodeData>;
};


/**
 * \brief Octree to organize vertices based on their position
 * \tparam VertexType Type of the vertices to store
 * \tparam VertexTraitsType Type to extract information from the vertices and their buffers
 * \tparam NodeData Extra data to store on each tree node
 */
template<
    class VertexType,
    class VertexTraitsType=VertexTraits<VertexType>,
    class NodeData=void
>
class Octree
{
public:
    /// Node type
    using value_type = OctreeNode<VertexType, VertexTraitsType, NodeData>;
    /// Vertex traits type
    using traits_type = typename value_type::traits_type;
    /// Vertex type
    using vertex_type = typename traits_type::vertex_type;
    /// Coordinate scalar type
    using scalar_type = typename traits_type::scalar_type;
    /// Coordinate vector type
    using coord_type = typename traits_type::coord_type;
    /// Vertex buffer type
    using shape_buffer_type = typename traits_type::shape_buffer_type;
    /// Vertex index type
    using index_type = typename traits_type::index_type;
    /// Size type
    using size_type = typename traits_type::size_type;
    /// Node iterator type
    using iterator = typename value_type::iterator;
    /// GlEnum corresponding to index_type
    static constexpr int gl_type = traits_type::gl_type;

    /// Type to represent an iterator range that goes though the leaf nodes
    class leaf_range
    {
    public:
        iterator begin()
        {
            return iterator(&tree->_top_node, true);
        }

        iterator end()
        {
            return iterator();
        }

    private:
        leaf_range(const Octree* tree) : tree(tree) {}
        const Octree* tree;
        friend Octree;
    };

    /**
     * \param max_node_size Size of the starting node
     * \param min_node_size Minimum size of a node, nodes smaller than this cannot be slpit
     * \param split_count   Number of vertices more than which a leaf node should be split
     * \param merge_count   Number of vertices fewer than which an inner node should be joined
     *                      defaults to split_count/2
     */
    explicit Octree(
        scalar_type max_node_size = 1024,
        scalar_type min_node_size = 1,
        size_type split_count = 1024,
        size_type merge_count = 0
    ) : _max_node_size(max_node_size),
        _min_node_size(min_node_size),
        _split_count(split_count),
        _merge_count(merge_count > 0 ? merge_count : split_count / 4),
        _top_node(
            {-max_node_size/2, -max_node_size/2, -max_node_size/2},
            {max_node_size/2, max_node_size/2, max_node_size/2}
        )
    {
    }

    /**
     * \pre size() == 0 && min.* < max.*
     * \post boundaries updated
     */
    void update_bounds(const coord_type& min, const coord_type& max)
    {
        _top_node._min = min;
        _top_node._max = max;
    }

    /**
     * \brief Inserts the indices in the tree
     * \param buffer        Buffer owning the vertices
     * \param from_index    First index to insert
     * \param to_index      Last index to insert (exclusive)
     * \post All the indices are inserted, the bounds of the tree might be
     *       increased to accomodate the data
     */
    template<class ContainerT>
    void add_indices(
        const ContainerT& buffer,
        index_type from_index,
        index_type to_index
    )
    {
        for ( ; from_index < to_index; from_index++ )
        {
            insert(buffer, from_index);
        }
    }

    /**
     * \brief Inserts the index in the tree
     * \param buffer        Buffer owning the vertices
     * \param from_index    Index to insert
     * \post The index is inserted, the bounds of the tree might be
     *       increased to accomodate the data
     */
    template<class ContainerT>
    void insert(
        const ContainerT& buffer,
        index_type index
    )
    {
        coord_type c = traits_type::coord(buffer[index]);
        while ( !_top_node.in_bounds(c) )
            _top_node = _top_node.expand(c);

        _top_node.insert(buffer, c, index, _split_count, _min_node_size);
    }

    /**
     * \brief Inserts the index in the tree without expanding the octree boundary
     * \param buffer        Buffer owning the vertices
     * \param from_index    Index to insert
     * \post The index is inserted if it's in bounds
     */
    template<class ContainerT>
    void insert_noexpand(
        const ContainerT& buffer,
        index_type index
    )
    {
        coord_type c = traits_type::coord(buffer[index]);
        if ( !_top_node.in_bounds(c) )
            return;

        _top_node.insert(buffer, c, index, _split_count, _min_node_size);
    }

    /**
     * \brief Removes indices from the tree
     * \param from_index    First index to remove
     * \param to_index      Last index to remove (exclusive)
     * \post The indices are no longer in the tree
     */
    void remove_indices(
        index_type from_index,
        index_type to_index
    )
    {
        _top_node.remove_indices(from_index, to_index, _merge_count, _max_node_size);
    }


    /**
     * \brief Removes all indices from the tree
     * \post The indices are no longer in the tree
     */
    void clear()
    {
        _top_node.clear(_merge_count, _max_node_size);
    }

    /**
     * \brief Octree boundary (minimum coordinates)
     */
    QVector3D min() const
    {
        return _top_node.min();
    }

    /**
     * \brief Octree boundary (minimum coordinates)
     */
    QVector3D max() const
    {
        return _top_node.max();
    }

    /**
     * \brief Size of the starting node
     */
    scalar_type max_node_size() const
    {
        return _max_node_size;
    }

    /**
     * \brief Minimum size of a node, nodes smaller than this cannot be slpit
     */
    scalar_type min_node_size() const
    {
        return _min_node_size;
    }

    /**
     * \brief Number of indices in the tree
     */
    size_type size() const noexcept
    {
        return _top_node.size();
    }

    /**
     * \brief Number of vertices more than which a leaf node should be split
     */
    size_type split_count() const
    {
        return _split_count;
    }

    /**
     * \brief Number of vertices fewer than which an inner node should be joined
     */
    size_type merge_count() const
    {
        return _merge_count;
    }

    /**
     * \brief Node iterator
     */
    iterator begin() const
    {
        return iterator(&_top_node, false);
    }

    /**
     * \brief Node iterator
     */
    iterator end() const
    {
        return iterator();
    }

    /**
     * \brief Iterator range that goes through all the leaves
     */
    leaf_range leaves() const
    {
        return {this};
    }

    /**
     * \brief Recursively invokes callback with all the nodes (depth-first)
     */
    template<class Callback>
    void update_node_data(const Callback& callback)
    {
        _top_node.update_data(callback);
    }

    Octree deep_copy() const
    {
        Octree copy(
            _max_node_size,
            _min_node_size,
            _split_count,
            _merge_count
        );
        copy._top_node = _top_node.deep_copy();
        return copy;
    }

    const value_type& root() const
    {
        return _top_node;
    }

private:
    scalar_type _max_node_size;
    scalar_type _min_node_size;
    size_type _split_count;
    size_type _merge_count;
    value_type _top_node;
    friend test_octree;
};

} // namespace glax3d

#endif // GLAX3D_OCTREE_H
