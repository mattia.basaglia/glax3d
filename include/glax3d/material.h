#ifndef GLAX3D_MATERIALS_MATERIAL_H
#define GLAX3D_MATERIALS_MATERIAL_H


#include "glax3d/shape_buffer.h"
#include "glax3d/shader.h"

namespace glax3d {

template<class MaterialT, class VertexT, class ShapeBufferT = ShapeBuffer<VertexT>>
class MaterialBase
{
public:
    using material_type = MaterialT;
    using vertex_type = VertexT;
    using shape_buffer_type = ShapeBufferT;


    void rendering_initialize(const QMatrix4x4& projection)
    {
        if ( !_loaded )
        {
            bind();
            if ( derived_this()->on_load(protected_tag()) )
            {
                _loaded = true;
            }

            if ( !shader().is_linked() )
            {
                throw Glax3DPublicError(QObject::tr("Shader not loaded"));
            }
        }
        derived_this()->on_rendering_initialize(projection, protected_tag());
    }

    /**
     * \brief sets up uniforms / attributes for the buffer
     * \pre buffer is bound
     */
    void prepare_buffer(const ShapeBufferT& buffer, const QMatrix4x4& model_transform)
    {
        derived_this()->on_prepare_buffer(buffer, model_transform, protected_tag());
    }

    Shader& shader() const
    {
        return derived_this()->on_get_shader(protected_tag());
    }

    void bind() const
    {
        shader().bind();
    }

    void unbind() const
    {
        shader().unbind();
    }

private:
    MaterialT* derived_this()
    {
        return static_cast<MaterialT*>(this);
    }

    const MaterialT* derived_this() const
    {
        return static_cast<const MaterialT*>(this);
    }

protected:
    struct protected_tag{};
    bool _loaded = false;

};

} // namespace glax3d

#endif // GLAX3D_MATERIALS_MATERIAL_H
